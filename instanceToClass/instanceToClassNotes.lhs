Replacing instance with class:

(>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
m >>= g = case m of
  Nothing -> Nothing
  Just x -> g x

class Class where

   m1 mp_1 :: ty1 

instance Class Type where

  m1 mp_1 = mrhs_1
   .
   .
   .
  m1 mp_n = mrhs_n


f1 fp_1 = frhs_1
  .
  .
  . 
f1 fp_n = frhs_n
