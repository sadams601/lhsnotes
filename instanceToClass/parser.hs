module Parser where
import Data.Char

data Expr = Var Char
          | N Int
          | Add Expr Expr
          | Sub Expr Expr
          | Assign Char Expr
          deriving Show

parse :: String -> (Expr,String)

parse (ch:chs) 
  | isAlpha(ch) = ((Var ch),chs)
  | isDigit(ch) = (N (fromEnum ch - fromEnum '0'), chs)
  | ch=='(' = (apply op, restFinal)
  | ch=='<' = (Assign v e, restFinal2)
      where
         (e1,rest1) = parse(chs)
         (op:rest2) = rest1
         (e2,rest3) = parse(rest2)
         (')':restFinal) = rest3
         apply '+' = Add e1 e2
         apply '-' = Sub e1 e2
         (v:':':rest4) = chs
         (e,restFinal2) = parse(rest4)

--Duplicating the state type here for practice and to avoid some of the complexity of MonadState/StateT stuff

newtype State s a = State { runState :: s -> (a,s) } 
  
instance Functor (State s) where
  fmap f sv = State (\s -> let (v, s2) = (runState sv) s in
                      ((f v), s2))

instance Applicative (State s) where
  pure a = State (\s -> (a,s))
  sf <*> sv = State (\s -> let (f, s2) = (runState sf) s
                               (v, s3) = (runState sv) s2
                           in ((f v), s3))

instance Monad (State s) where
  return = pure
  s1 >>= f = State (\s -> let (a, s2) = (runState s1) s
                              newState = f a
                          in  (runState newState) s2)

evalState :: State s a -> s -> a
evalState comp s = fst (runState comp s)
             
get :: State s s
get = State (\s -> (s,s))

put :: s -> State s ()
put newS = State (\_ -> ((),newS))


--parse :: String -> (Expr,String)
-- Expr is the result 'a'
-- String is the state
parseM (ch:chs)

{-
  | isAlpha(ch) = ((Var ch),chs)
  | isDigit(ch) = (N (fromEnum ch - fromEnum '0'), chs)
The first two cases are fairly simple to refactor. Though we somehow need to infer that the second element of the tuple is the state so
'chs' needs to be 'put'
-}

  | isAlpha(ch) = do
      put chs
      return (Var ch)
  | isDigit(ch) = do
      put chs
      return (N (fromEnum ch - fromEnum '0'))
-- Presumably the approach to this case is to find the definitions of 'op' and 'restFinal'. Perform a where to let binding first perhaps? 
  | ch=='(' = do
      e1 <- parseM chs
      (op:rest2) <- get
      e2 <- parseM rest2
      (')':restFinal) <- get
      put restFinal
      return (apply op e1 e2)
  | ch=='<' = do
      let (v:':':rest) = chs
      e <- parseM rest
      return (Assign v e)
      where
         apply '+' e1 e2 = Add e1 e2
         apply '-' e1 e2 = Sub e1 e2
         
