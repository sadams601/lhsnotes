> module OtherMonadGen where
> import Control.Monad.IO.Class
> import System.Directory (doesDirectoryExist, getDirectoryContents)
> import System.FilePath ((</>))
> import Control.Monad (forM,liftM, forM_, when)
> import Control.Monad.Trans (liftIO, MonadTrans,lift)
> import Control.Monad.Writer (WriterT, tell, execWriterT)

This file will take a look at generalising some functions using monads other than Maybe.

IO looks like it will be very simple due to the liftIO function.

liftIO :: MonadIO m => IO a -> m a

From https://www.haskell.org/tutorial/io.html

> sequenceIO        :: [IO ()] -> IO ()
> sequenceIO []     =  return ()
> sequenceIO (a:as) =  do a
>                         sequenceIO as

> sequenceIO_ref :: (Monad m) => [m ()] -> m ()
> sequenceIO_ref [] = return ()
> sequenceIO_ref (a:as) = do a
>                            sequenceIO_ref as

This refactors so nicely that the name is really inaccurate now! This is a less general version of sequence_ from Data.Foldable.

sequence_ :: (Foldable t, Monad m) => t (m a) -> m ()
sequence_ = foldr (>>) (return ())

From Chapter 18 of Real World Haskell

> listDirectory :: FilePath -> IO [String]
> listDirectory = liftM (filter notDots) . getDirectoryContents
>   where notDots p = p /= "." && p /= ".."

WriterT has the signature WriterT w m a, w is the type of values to be recorded, a is the type the monad is operating over, m is the monad whos behaviour is being modified by the transformer.

> countEntries :: FilePath -> WriterT [(FilePath, Int)] IO ()
> countEntries path = do
>   contents <- liftIO . listDirectory $ path
>   tell [(path, length contents)]
>   forM_ contents $ \name -> do
>     let newName = path </> name
>     isDir <- liftIO . doesDirectoryExist $ newName
>     when isDir $ countEntries newName

Created a simple top level function to demonstrate the refactoring.

> runCountEntries :: IO ()
> runCountEntries = do
>   let fp = ".."
>   res <- execWriterT $ countEntries fp
>   print res
   
If we wanted to generalise countEntries over WriterT to work with any tranformer

The only subexpression that needs to be generalised is tell.

tell :: Monad m => w -> WriterT w m ()

countEntries_ref1 :: (MonadTrans t, Monad m) => ([(FilePath, Int)] -> t [(FilePath, Int)] IO ()) -> FilePath -> t [(FilePath, Int)] IO ()

Need to figure out why the above type doesn't work but the one below does.

> countEntries_ref1 :: MonadIO m => ([(FilePath, Int)] -> m a) -> FilePath -> m ()
> countEntries_ref1 tAct path = do
>   contents <- liftIO . listDirectory $ path
>   tAct [(path, length contents)]
>   forM_ contents $ \name -> do
>     let newName = path </> name
>     isDir <- liftIO . doesDirectoryExist $ newName
>     when isDir $ countEntries_ref1 tAct newName

> countEntries_ref15 :: (MonadTrans t) => ([(FilePath, Int)] -> t IO a) -> FilePath -> t IO ()
> countEntries_ref15 tAct path = do
>   contents <- lift . listDirectory $ path
>   tAct [(path, length contents)]
>   forM_ contents $ \name -> do
>     let newName = path </> name
>     isDir <- lift . doesDirectoryExist $ newName
>     when isDir $ countEntries_ref15 tAct newName

> runCountEntries_ref1 :: IO ()
> runCountEntries_ref1 = do
>   let fp = ".."
>   res <- execWriterT $ countEntries_ref1 tell fp
>   print res

Trying to generlise over the monad...

mAct1 is liftIO and in the general function this will have to be a lift of some sort as well
mAct2 is listDirectory
mAct3 is doesDirectoryExist
(m a -> WriterT [(FilePath,Int)] m a) ->

.> countEntries_ref2 :: (Monad m) => (FilePath -> m [String]) -> (FilePath -> m Bool) -> FilePath -> WriterT [(FilePath, Int)] m a

 countEntries_ref2 mAct2 mAct3 path = do
   contents <- lift . mAct2 $ path
   tell [(path, length contents)]
   forM_ contents $ \name -> do
     let newName = path </> name
     isDir <- lift . mAct3 $ newName
     when isDir $ countEntries_ref2 mAct2 mAct3 newName

 runCountEntries_ref2 :: IO ()
 runCountEntries_ref2 = do
   let fp = ".."
   res <- (execWriterT $ countEntries_ref2 listDirectory doesDirectoryExist fp) :: [(FilePath, Int)]
   print res

I can't get this to work the types are getting super complex. It's also not a great candidate for this refactoring in my opinion because there are a lot of IO types that need to be generalised.
Trying to generalise IO to monad in count entries again.

liftIO becomes lift

listDirectory :: FilePath -> IO [String]

doesDirectoryExist :: FilePath -> IO Bool

> countEntries_ref2 :: (Monad m) => (FilePath -> m [String]) -> (FilePath -> m Bool) -> FilePath -> WriterT [(FilePath,Int)] m ()
> countEntries_ref2 mAct1 mAct2 path = do
>   contents <- lift . mAct1 $ path
>   tell [(path, length contents)]
>   forM_ contents $ \name -> do
>     let newName = path </> name
>     isDir <- lift . mAct2 $ newName
>     when isDir $ countEntries_ref2 mAct1 mAct2 newName
> 
> runCountEntries_ref2 :: IO ()
> runCountEntries_ref2 = do
>   let fp = ".."
>   res <- execWriterT $ countEntries_ref2 listDirectory doesDirectoryExist fp
>   print res
