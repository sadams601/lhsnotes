module ThesisExamples where
import Control.Applicative hiding ((<|>), many, optional)
import Control.Monad
import Text.ParserCombinators.Parsec

data Currency = Dollar
              | Pound
              | Euro
              deriving (Show,Eq)
              
data Money = M Currency Integer Integer
           deriving (Show,Eq)

readWhole = read <$> many1 digit

parseCurrency :: CharParser () Currency
parseCurrency = parsePound <|> parseDollar <|> parseEuro
   where parsePound = Pound <$ char '£'
         parseDollar = Dollar <$ char '$'
         parseEuro = Euro <$ char '€'

parseMoney :: CharParser () Money
parseMoney = M <$> parseCurrency <*> readWhole <*> (char '.' *> readWhole)
