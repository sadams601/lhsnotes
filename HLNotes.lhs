> {-# LANGUAGE ScopedTypeVariables #-}
> module HList3 where
> import qualified Data.DList as D 
> import Control.Monad

--Super contrived example

> explode :: Int -> [a] -> [a]
> explode n lst = concat (map (\x -> replicate n x) lst)

We know the refactored type of explode will be:
   explode :: Int -> [a] -> DList a

Since the refactored argument is the final type of explode we will traverse the AST in a top down left to right manner

At this point we look at the AST which is (roughly):

==REFACTORING LHS1

        LHS1                                   RHS1
(HsApp "concat :: foldable t => t [a] -> [a]" "(map (\x -> replicate n x) lst) :: [a]") :: (currently :: [a] should be :: DList a)

Looking first at LHS1: we have associated "concat" with the DList function "D.concat :: [DList a] -> DList a" since the D.concat
functions rightmost type is our goal type we substitute "concat" from the LHS of this application with "D.concat."

We have now fixed the top level application and LHS1 based on the preconceived notions of the relationship between functions that operate over
lists and those that operate over DLists and the desired output type. We still need to figure out what the definition of "RHS1" is
because its desired type doesn't match the current expressions derived type.
We start by descending this branch:

==REFACTORING RHS1 = (map (\x -> replicate n x) lst)

        LHS2                                        RHS2
(HsApp "map (\x -> replicate n x) :: [a] -> [[a]]" "lst :: [a]") (currently :: [[a]] should be :: [DList a])

Once again we start on the LHS first with the goal of refactoring it so that it is of type (Top -> [DList a])
Once we rewrite the LHS expression its first type will be more constrained but we don't worry about that for now.
When we refactor the RHS expression this constrained type will be the "goal" type.

However "map (\x -> replicate n x)" doesn't match any of our allowed replacements so we will continue decomposing LHS2

==REFACTORING RHS2
        LHS3                           RHS3
(HsApp "map:: (a -> b) -> [a] -> [b]" "(\x -> replicate n x) :: a -> [a]") (currently :: [a] -> [[a]] should be :: (Top -> [DList a])

NOTE: I think there are two options here the simplest rewriting is to just replace "replicate" in the RHS with "D.replicate" and then the refactoring is complete.
      This would require for the refactoring to recognize that the type of RHS2 can be affected by changing RHS3, not sure how to do this exactly.
      Otherwise lets continue in the way we have been...

Starting with LHS3 we have found a replacement for "map" with "D.map :: (a -> b) -> DList a -> DList b" however "DList b" cannot match our goal type for RSH2 so
we cannot perform the replacement. Bit stuck now... End up with the same problem of how does the refactoring figure out that RHS3's type will affect the type of RHS2 without
changing LHS3?


====Bottom up example =====

Now lets refactor explode to the following type:

explode :: Int -> DList a -> [a]

This time we will traverse the AST from the bottom up. We descend the tree to all the leaves that correspond to the argument being refactored (in this case "lst :: [a]")

When refactoring the third type of explode I want to start thinking of the refactoring in concrete tree traversals.

One case is trivial; when we check the type of "concat (map (\x -> replicate n x) lst)"
and it is currently "[a]" but needs to become "DList a". However we already have a function that projects [a] onto DList a "fromList."
The entire RHS can be wrapped in fromList and the refactoring can be considered finished.

> explode2 :: Int -> [a] -> D.DList a
> explode2 n lst = D.fromList (concat (map (\x -> replicate n x) lst))

This version is perhaps a bit unsatisfactory if the programmer wanted more of the function work over DLists

What if instead we traverse the tree but make "fromList" only used on a parameter

We start with the definition of the RHS and at that level there are two sections

LHS : concat :: foldable t => t [a] -> [a]

RHS: (map (\x -> replicate n x) lst) :: [[a]]

Lets say that we define the refactoring so that "concat" can be replaced with "D.concat" so we refactor the LHS to be (D.concat :: [D.DList a] -> D.DList a)

This means that the RHS will need to be retyped as [DList a] so now our refactoring will focus on retyping the RHS

explode1 :: Int -> [a] -> D.DList a
explode1 n lst = D.fromList (concat (map (\x -> replicate n x) lst))

{-

This version doesn't work because D.concat :: [DList a] -> DList a
but (D.fromList (map (\x -> replicate n x) lst)) :: DList [a]

explode2 :: Int -> [a] -> D.DList a
explode2 n lst = D.concat (D.fromList (map (\x -> replicate n x) lst))
-}


{-
explode2 :: Int -> [a] -> DList a
explode2 n lst = join (D.map (\x -> D.replicate n x) (D.fromList lst))

explode3 :: Int -> [a] -> DList a
explode3 n lst = D.concat 

concat :: foldable t => t [a] -> [a]

D.concat :: [DList a] -> DList a

-}

f :: IO ()
f = do
  (lst :: [Int]) <- read <$> getLine
  let newLst = D.toList (explode1 3 lst)
  print newLst


> explode_ref :: Int -> D.DList a ->  [a]
> explode_ref n lst = concat (D.map (\x -> replicate n x) lst)

> explode_ref2 :: Int -> D.DList a -> [a]
> explode_ref2 n lst = D.toList (D.concat (D.toList (D.map (\x -> D.replicate n x) lst)))


D.map (\x -> D.replicate n x) :: D.DList a -> D.DList (D.DList a)
This is bad because it 
