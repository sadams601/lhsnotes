>{-# LANGUAGE UndecidableInstances #-}
> module Step1B where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Reader
> import Control.Monad.Except

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String

> parse :: String -> ParseState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     put restFinal
>     return (Assign v e)
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div


Eventual type of parser

String -> StateT String (Except String) Expr

String -> ExceptT String (State String) Expr


Proposed steps for parse :: String -> StateT String (Except String) Expr
1. Monad to monad tranformer transformation
2. Specialise monad m to Except String
3. User introduces the exceptions they want

In this case parse will become

parse :: (Monad m) => String -> ExceptT ParseState Expr

We have to assert that (t ParseState) is a monad to be able to use (lift . put) within a do block.

> parse_ref :: (MonadTrans t, Monad (t ParseState)) => String -> t ParseState Expr
> parse_ref (ch:chs)
>   | isAlpha(ch) = do
>     (lift . put) chs
>     return (Var ch)
>   | isDigit(ch) = do
>     lift $ put chs
>     return (N (fromEnum ch - fromEnum '0'))

> instance (MonadIO m, MonadTrans t, Monad (t m)) => MonadIO (t m) where
>   liftIO = lift . liftIO


