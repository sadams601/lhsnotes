> {-# LANGUAGE FlexibleContexts #-}
> module ReaderWork where
> import Control.Monad.Reader

This code is taken from Real World Haskell starting on pg. 432

> myName step = do
>   name <- ask
>   return (step ++ ", I am " ++ name)

> localExample :: Reader String (String, String, String)
> localExample = do
>   a <- myName "First"
>   b <- local (++"dy") (myName "Second")
>   c <- myName "Third"
>   return (a, b, c)
