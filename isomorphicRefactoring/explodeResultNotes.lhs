> module ExpRes where
> import Data.DList (DList)
> import qualified Data.DList as DList

I'm going to be working with the following function:

> explode :: Int -> [a] -> [a]
> explode n lst = concat (map (\x -> replicate n x) lst)

I will be refactoring explodes third type (the result type)

In the same way that I did in the intro_cas1.lhs document in this directory.

Here is explode's AST

                       BIND: Var: explode                      
                               |                               
                              App                              
                               |                               
      -------------------------------                          
     /                               \                         
Var: concat                         App                        
                                     |                         
                                 -------------------------     
                                /                         \    
                               App                     Var: lst
                                |                              
                 --------------------                          
                /                    \                         
             Var: map               Lam                        
                                     |                         
                                    App                        
                                     |                         
                                  ---------------              
                                 /               \             
                                App            Var: x          
                                 |                             
                              -----------                      
                             /           \                     
                       Var: replicate  Var: n   

We know that the result type of the LHS needs to become DList a
We descend the LHS and find the var "concat :: Foldable t => t [a] -> [a]"

concat's result type is incorrect but we can replace it with "DList.concat :: [DList a] -> DList a"

Now we can go down the RHS with it's goal type being [DList a]


                          App                        
                           |                         
                       -------------------------     
                      /                         \    
                     App                     Var: lst
                      |                              
       --------------------                          
      /                    \                         
   Var: map               Lam                        
                           |                         
                          App                        
                           |                         
                        ---------------              
                       /               \             
                      App            Var: x          
                       |                             
                    -----------                      
                   /           \                     
             Var: replicate  Var: n

Since the goal type is determined by the furthest left we descent directly to "map :: (a -> b) -> [a] -> [b]"

This goal type will work as long as the type variable b is eqivalent to "DList a"

b is the result type of the lambda so we descend that tree again to the furthest left child to check it's return type.

In this case it's "replicate :: Int -> a -> [a]" which can be replaced with "DList.replicate :: Int -> a -> DList a" which matches our needed result type. so we make the switch. The two parameters of replicate and DList.replicate match so we can skip checking those subtrees and finish the refactoring.

The new definition of explode is:

> explode_ref :: Int -> [a] -> DList a
> explode_ref n lst = DList.concat (map (\x -> DList.replicate n x) lst)


Questions needing answers:

Do the type of Ids have their type variables replaced after typechecking?
No they don't which is good. Here is the type ast of map as pulled from the definition of explode

map :: (a -> b) -> [a] -> [b]

   (ForAllTy {Var: a} 
    (ForAllTy {Var: b} 
     (FunTy 
      (FunTy 
       (TyVarTy {Var: a}) 
       (TyVarTy {Var: b})) 
      (FunTy 
       (TyConApp 
        ({abstract:TyCon}) 
        [
         (TyVarTy {Var: a})]) 
       (TyConApp 
        ({abstract:TyCon}) 
        [
         (TyVarTy {Var: b})])))))


And for example's sake here is concat's type

concat :: Foldable t => t [a] -> [a]

   (ForAllTy {Var: t} 
    (ForAllTy {Var: a} 
     (FunTy 
      (TyConApp 
       ({abstract:TyCon}) 
       [
        (TyVarTy {Var: t})]) 
      (FunTy 
       (AppTy 
        (TyVarTy {Var: t}) 
        (TyConApp 
         ({abstract:TyCon}) 
         [
          (TyVarTy {Var: a})])) 
       (TyConApp 
        ({abstract:TyCon}) 
        [
         (TyVarTy {Var: a})])))))


Things to make:

When I make a swap I should be able to check what subtrees need changing e.g.

when swapping

replicate :: Int -> a -> [a]
for
DList.replicate :: Int -> a -> DList a

we know that we don't need to check the two argument subtrees because those types haven't changed
