> import Data.DList (DList)
> import qualified Data.DList as DList

Ok there are three different cases for an isomorphic type changing refactoring.

The cases depend on which type is the focus of the refactoring.

The three options are:

1. The type to be refactored is the result type, e.g.:
  Parameters -> A

2. The type to be refactored is a parameter, e.g.:
  A -> ResultType

3. The type to be refactored is both a parameter and the result type, e.g.:
  A -> A 
  --Note in this case there could be other parameters not the target of the refactoring


This document will walk through examples of each of the three cases, refactoring
the list type to DLists.

CASE 1
===========================================================================================

This case will refactor the result type of the exponents function. Seen below:

> exponents :: Int -> [Int]
> exponents base = base : (exponents (2*base))

A simplified version of exponents AST can be seen below.

            BIND: Var: exponents :: Int -> [Int]            
                     |                      
                   Op: (:) 
                     |                      
     ----------------------                 
    /                      \                
Var: base                 App               
                           |                
                  -----------------         
                 /                 \        
           Var: exponents      Op: Var: (*)
                                   |        
                              ---------     
                             /         \    
                           Lit: 2  Var: base

This refactoring will change the type of exponents to:

exponents :: Int -> DList Int

Since we are refactoring the result type we start at the top of the AST:

                   Op: (:) :: a -> [a] -> [a]
                     |                      
                   ------
                  /      \
              LHS_1     RHS_1


From exponents new type we know that the result type of this top level needs to be
(DList Int) there are two options

1. Replace (:) with its DList equivalent DList.cons
2. Wrap (:) with DList.fromList
   -- Note this actually wouldn't work because of the recursive call but we don't know that at this point

Option 1 is prefered in this case because it moves the new type further down the function
The AST now looks like:
                   App
                    |
                --------
               /       \
         DList.cons    App
                        |                      
                     ------
                    /      \
                LHS_1     RHS_1
      

Given the type environment of

exponents :: Int -> DList Int
DList.cons :: a -> DList a -> DList a

We can infer the following things

a :: Int
LHS_1 :: Int
RHS_1 :: DList Int

Now we can figure out the new definitions of LHS_1 & RHS_1 independently

Starting with LHS_1 which is just the variable "base"

Since our goal type is now Int and base is also an Int we can just return
without modifying the definition of LHS_1

Onto RHS_1 with a goal type of (DList Int)
Currently RHS_1's AST looks like this:

                          App               
                           |                
                  -----------------         
                 /                 \        
           Var: exponents      Op: Var: (*)
                                   |        
                              ---------     
                             /         \    
                           Lit: 2  Var: base

Like with the previous application we'll move through the subtrees from left to right

We have to traverse the tree in this order because all we know at this point is the goal type
which is determined by the result type of the LHS

The LHS consists just of the variable "exponents"

This is the recursive call so the new definition's type of Int -> DList Int is the actual type of the variable

NOTE: When I'm implementing this the type information on this identifier will not be updated

Since exponents result type is the same as the goal type nothing needs to change.

Now we just need to ensure that the RHS of this application is an Int which it is.

This application is already the goal type so we can return an write out the new function

> exponents_ref :: Int -> DList Int
> exponents_ref base = DList.cons base (exponents_ref (2*base))
