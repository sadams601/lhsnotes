> import Data.DList (DList)
> import qualified Data.DList as DList
> import Prelude hiding (sum)


This file will cover the second type of isomorphic refactoring where 
the refactoring will only affect the type of one of the parameters and not the result type

We'll be refactoring the first argument of sumEvens

This is a bit of a contrieved function but we can imagine it being the result of a previous
refactoring that replaced pattern matching with the calls to head and tail

> sumEvens :: [Int] -> Int
> sumEvens lst = case (head lst) `mod` 2 of
>   0 -> (head lst) + sumEvens (tail lst)
>   1 -> sumEvens (tail lst)

sumEvens ast looks like so:


                                                  BIND: Var: sumEvens                                                   
                                                           |                                                            
                                                          Case                                                          
                                                           |                                                            
              ----------------------------------------------------------------------------------------                  
             /                                          |                                             \                 
      Case Expression                                Match: 0                                      Match: 1             
             |                                          |                                             |                 
          Op:  mod                                    Op:  +                                         App                
             |                                          |                                             |                 
          -------------                ---------------------------                           -----------------          
         /             \              /                           \                         /                 \         
        App          Lit: 2          App                         App                  Var: sumEvens          App        
         |                            |                           |                                           |         
     ---------                    ---------              -----------------                                ---------     
    /         \                  /         \            /                 \                              /         \    
Var: head  Var: lst          Var: head  Var: lst  Var: sumEvens          App                         Var: tail  Var: lst
                                                                          |                                             
                                                                      ---------                                         
                                                                     /         \                                        
                                                                 Var: tail  Var: lst     

In case 1 we just work on moving the new type down through the AST
but in case 2 we just work to fix any typing errors that are introduced by changing the type of the parameter.

In this case we will descend the AST keeping track of what type each subtree needs to have. SimEvens is a case statement at the
top level so we need to ensure that the type of the case expression is the same as the type of the left hand side matches.

We also need to make sure that the type of the right hand side of each match matches the result type of sumEvens.

We'll start by checking the case expression itself.


          Op:  mod           
             |               
          -------------      
         /             \     
        App          Lit: 2  
         |                   
     ---------               
    /         \              
Var: head  Var: lst        

IMPLEMENTATION NOTE: I think I need to pull the type the the case expression is supposed to be from the
   type annotations on the lhs of the matches.

The entire case expression needs to be of type Int

The top of the case expression is an infix call to mod :: Integral a => a -> a -> a
Because we know that the result type of this expression needs to be of type Int we can specify
that a needs to be Int as well

This means that the two sub-expressions must also be of type Int.

Descending down the LHS first we can see that one of this application's immediate subchildren
is the refactored argument lst so three things will occur at this point

1. No change needs to happen because of a polymorphic function on the lhs that accepts
   the DList type
2. There is an equivalent definition of the lhs that accepts the refactored type
3. If neither 1 or 2 are true then we will need to wrap lst with a call to the abstraction function
   toList

In this case option 2 works because DList also has a head operation so the AST now becomes:

          Op:  mod           
             |               
          -------------      
         /             \     
        App          Lit: 2  
         |                   
     --------------               
    /              \              
Var: DList.head  Var: lst

Since DList.head lst :: Int which is the target type this new case expression types and we can move on.

We also check that the left hand side of the call to mod type checks as well.

Moving onto the first of the match cases now:

                   
                        Match: 0
                            |
                         Op:  +                                                                                         
                           |                                                                                            
          ---------------------------                                                                                   
         /                           \                                                   
        App                         App                
         |                           |                 
     ---------              -----------------          
    /         \            /                 \         
Var: head  Var: lst  Var: sumEvens          App        
                                             |         
                                         ---------     
                                        /         \    
                                    Var: tail  Var: lst


With the match cases we know that the entire expression needs to have the same type as the
result type of the entire function. Which in this case is Int



Finally we must check the second match case:

             Match: 1             
                |                 
               App                
                |                 
       -----------------          
      /                 \         
Var: sumEvens          App        
                        |         
                    ---------     
                   /         \    
               Var: tail  Var: lst
