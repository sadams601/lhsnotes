> module TyclassGen where

Since doing the maybe to monad plus refactoring I think it would be interesting to look at two other refactorings, one that generalises Maybe to anther type class, and one that generalises another type to the monad plus typeclass.

When you switch to a different typeclass this may not change the refactoring in any way other than what the type becomes. For example Alternative declares two methods, empty and (<|>) for maybe at least empty is equivalent to MonadPlus's mzero and (<|>) is equivalent to mplus.

Ah this is by design however because the declaration of MonadPlus defines its methods in terms of Alternative if possible.

I think in general generalisation is the most effective/ makes the most sense when the function doesn't use any of the type's features, just typeclass methods (or at the very least the code can be reduced to more gneral types)

What if there was an automatic way of detecting when code could be generalised?

Monad plus for list:

mzero = []
mplus = (++)

for reference:

Monad plus for Maybe
mzero = Nothing

Nothing `mplus` r = r
l `mplus` _ = l

The actual definition is automatically generated because Maybe is also "Alternative"
See code here: https://hackage.haskell.org/package/base-4.8.2.0/docs/src/GHC.Base.html#line-707

Also here is a discussion on the relationship between Monoid, Alternative, and MonadPlus on stack overflow
http://stackoverflow.com/questions/13080606/confused-by-the-meaning-of-the-alternative-type-class-and-its-relationship-to


