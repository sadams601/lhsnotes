> module MonadicJsonParser where
> import Control.Applicative hiding ((<|>), many, optional)
> import Control.Monad
> import Text.ParserCombinators.Parsec
> import Data.Map

Grabbing the JSON data object from the applicative version 

> data JSON = JArr [JSON]
>           | JObj (Map String JSON)
>           | JStr String
>           | JNum Double
>           | JBool Bool
>           | JNull
>             deriving (Show, Eq)
>

> jsonParse :: CharParser () JSON
> jsonParse = do
>   spaces
>   (parseJObj <|> parseJArr)

> parseJObj :: CharParser () JSON
> parseJObj = do
>   char '{'
>   spaces
>   entries <- parseObjEntries
>   spaces
>   char '}'
>   return $ JObj entries
>   where
>     parseObjEntries = do
>       lst <- objEntry `sepByIgnoreSpaces` char ','
>       return $ fromList lst
>     objEntry = do
>       spaces
>       str <- parseStr
>       spaces
>       char ':'
>       spaces
>       val <- parseJVal
>       spaces
>       return (str,val)
>
> parseJVal = do {b<-parseBool;return $ JBool b} <|>
>             do {d<-parseDouble;return $ JNum d} <|>
>             do {string "null"; return JNull} <|> do {s<-parseStr;return $ JStr s}

> parseJArr :: CharParser () JSON
> parseJArr = do
>   char '['
>   spaces
>   vals <- parseJVal `sepByIgnoreSpaces` char ','
>   spaces
>   char ']'
>   spaces
>   return $ JArr vals
>
> parseDouble :: CharParser () Double
> parseDouble = do
>   whole <- many1 digit
>   decimal <- option "" (do {
>                           char '.';
>                           decs <- many1 digit;
>                           return ('.':decs)})
>   return $ read (whole++decimal)
>

> sepByIgnoreSpaces :: CharParser () a -> CharParser () sep -> CharParser () [a]
> sepByIgnoreSpaces p sep = p `sepBy` (try (do {
>                                              spaces;
>                                              s <- sep;
>                                              spaces;
>                                              return s}))

do
  char '{'
  x <- thing we care about


> parseStr :: CharParser () String
> parseStr = do
>   char '"'
>   str <- many1 (noneOf "\"")
>   char '"'
>   return str

Can't figure out a way to do this one really monadically

> parseBool :: CharParser () Bool
> parseBool = parseTrue <|> parseFalse
>    where parseTrue = True <$ string "true"
>          parseFalse = False <$ string "false"
