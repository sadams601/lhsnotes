module TypeUtils where
import Data.Typeable

--The point of this module is to start working on utils that help analyse the types of haskell expressions.

--Some of this code may be a candidate for insertion into HaRe proper. Other parts may repeat work HaRe has already done.

getStaticType :: Typeable a => a -> String
getStaticType = show . typeOf


main :: IO ()
main = do
  
