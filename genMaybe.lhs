> module GenMaybe where
> import Control.Monad

The idea behind this module is to enumerate the different cases that one could run into when running this refactoring. The easiest cases are

> simpMaybe :: (Integral a) => a -> Maybe a
> simpMaybe 0 = Nothing
> simpMaybe x = Just x

The obvious solution

> simpMaybeRef1 :: (Integral a, Monad m) => a -> m a
> simpMaybeRef1 0 = fail "Failure case inserted by generlise maybe to monad"
> simpMaybeRef1 x = return x

Nothing will be mapped to fail in this case the string could be customised to be more informative if need be

Another easy case

> collapseExpr1 :: Maybe a -> Maybe a
> collapseExpr1 m = case m of
>   Nothing -> Nothing
>   Just x  -> Just x

> collapseExprRef1 :: (Monad m) => m a -> m a
> collapseExprRef1 m = m >>= (\x -> return x)

In this case we can just pass the input parameter into bind. The variable name 'x' comes from the Just pattern match from the original function. The refactoring should preserve this variable name.

An equivalent way to define the original function which should be refactored in the same way.

> collapseExpr2 :: Maybe a -> Maybe a
> collapseExpr2 Nothing = Nothing
> collapseExpr2 (Just x) = Just x

In general when both the Nothing and just cases are matched and the output type is also Maybe the refactoring is

lhs >>= (\x -> return expr)

where lhs is the input maybe value
       x  is the variable name that was pattern matched on Just
       expr was the rhs of the Just pattern

Other cases need to look at

f :: (Monad m) => Maybe a -> m a
f :: (Monad m) => m a -> Maybe a

^Look at binds and do syntax

Lets say we are mapping Maybe onto Either

> f :: Maybe a -> Either String a
> f Nothing  = Left "Computation has failed" 
> f (Just x) = Right x

How do you deal with maybes on the LHS and another monad on the RHS?

fr :: (Monad m) => m a -> Either String a
fr ma = ma >>= (\x -> Right x) 

With this solution the user loses the information contained in the string
Is there a way to do this? Maybe transformers? See SO http://stackoverflow.com/a/19427925/1245646

If we are just generlising to MonadPlus however the Left case can be maintained by pattern matching on mzero:

fr2 :: (MonadPlus m) => m a -> Either String a
fr2 mzero = Left "Computation has failed"
fr2 ma = ma >>= (\x -> Right x)

The above attempts wont work because bind doesn't allow the type of the monad to change. Instead this is where a projection function will have to be added

> fr :: (MonadPlus m) => (m a -> a) -> m a -> Either String a
> fr _ mzero = Left "Computation has failed"
> fr proj mx = Right (proj mx)

> inv :: Int -> Maybe Int
> inv 0 = Nothing
> inv i = Just (1 `div` i)

> inv_r :: (MonadPlus m) => Int -> m Int
> inv_r 0 = mzero
> inv_r i = return (1 `div` i)
