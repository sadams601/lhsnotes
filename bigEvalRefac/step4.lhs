> module Step4 where
> import Data.Char
> import Control.Monad.Reader
> import Control.Monad.State

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           deriving Show

> type Env = [(Char, Int)]
> type EvalSt = StateT Env
 
> eval :: (Monad m) => Expr -> StateT Env m (Maybe Int)
> eval (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)
> eval (Assign x e) = do
>   env <- get
>   v <- eval e
>   put $ (x,v):env
>   return v

The fourth step is to specialise m to maybe. The type signature will need to be (eval :: Expr -> EvalSt Maybe (Maybe Int)) because we can't be sure that there is a result in the end once we add division.

 eval_ref :: Expr -> EvalSt Maybe Int
 eval_ref (Var v) = do
   env <- get
   return (head [val | (x,val) <- env, x==v])
 eval_ref (N n) = return n
 eval_ref (Add e1 e2) = do
   v1 <- eval_ref e1
   v2 <- eval_ref e2
   return (v1 + v2)
 eval_ref (Sub e1 e2) = do
   v1 <- eval_ref e1
   v2 <- eval_ref e2
   return (v1 - v2)
 eval_ref (Assign x e) = do
   env <- get
   v <- eval_ref e
   put $ (x,v):env
   return v

This is not how this should be implemented. We aren't specialising m we are modifying the return type. The type should actually be

eval_ref :: (Monad m) => Expr -> EvalSt m (Maybe Int)

This is because we aren't sure if the expression can be evaluated because there might have been a divide by zero rather than wanting to execute code within a monad m. 

> type Env_ref = [(Char, (Maybe Int))]
> type EvalSt_ref = StateT Env_ref

> eval_ref :: (Monad m) => Expr -> EvalSt_ref m (Maybe Int)
> eval_ref (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> eval_ref (N n) = return $ return n
> eval_ref (Add e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return $ v1 >>= (\x1 -> v2 >>= (\x2 -> return (x1 + x2)))
> eval_ref (Sub e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return $ v1 >>= (\x1 -> v2 >>= (\x2 -> return (x1 - x2)))
> eval_ref (Assign x e) = do
>    mv <- eval_ref e
>    env <- get
>    put ((x,mv):env)
>    return mv

This one was pretty tricky to get right. 

When we change the return type of the eval this will also change Env because the expression whos value is being assigned to a variable could be Nothing.

In general when refactoring this computation from type (t m a) to type (t m Maybe a)

f = (do
 v <- f
 h v
 return $ (g v)) :: a

f :: m a

=refac=>
f = (do
  mv <- f
  mv >>= (\v -> h v)
  return $ mv >>= (\v -> return $ g v))

f :: Maybe a

Steps for this refactoring

1. Rename  definition of "var" to prepend m making new name "mVar".
2. All expressions "e" which contain references to "var" become
    mVar >>= (\var -> return $ e)



