> module Step1 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Reader

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           deriving Show

> type ParseState = State String

> parse :: String -> ParseState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>       where apply '+' = Add
>             apply '-' = Sub

> prsStr = "(((x+2)-4)+(y+x))"

> env1 :: [(Char,Int)]
> env1 = [('x',5),('y',2)]

> type Env = [(Char,Int)]

> type EvalRdr = Reader Env

> eval :: Expr -> EvalRdr Int
> eval (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)

This is our eval code using just the reader monad and the goal is to refactor it to be of type Expr -> StateT (Env Int) Maybe Int.

My proposed refactoring order is:

1. Monad to MonadTrans refactoring: aka transform Reader to ReaderT
2. Generalise ReaderT to MonadTrans t
3. MonadTrans specialized to StateT
4. Introduce Maybe

The issue with this ordering is that we generalise so much it's hard to keep the structure of the transformer vs the monad straight. We could instead look at things like

1. Generalise Reader to Monad m
2. Introduce Assign constructor to Expr and add eval case
3. Specialise eval to State
4. Promote State to StateT
5. Introduce Div to Expr and add eval case

Problem with first step, we can't fully generalise Reader to Monad m because then the state is lost.

1. Reader Env to ReaderT Env m a transformation
2. Transform ReaderT to StateT
3. Introduce Assign constructor to Expr and add eval case
4. Specify m to be maybe
5. Introduce Div to Expr add eval case

> type EvalRdr_ref = ReaderT Env

> eval_ref :: (Monad m) => Expr -> EvalRdr_ref m Int
> eval_ref (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x==v])

Need to be careful with the previous step. In this case ask can remain because it is defined for both Reader and ReaderT. I'm guessing this is something we could count on going forward when transforming Monads to Monad transformers that there will be the same function names in the transformer as in the monad.

Rest of eval

> eval_ref (N n) = return n
> eval_ref (Add e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return (v1 + v2)
> eval_ref (Sub e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return (v1 - v2)

We are now ready for step 2.
