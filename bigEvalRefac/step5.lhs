> module Step5 where
> import Data.Char
> import Control.Monad.Reader
> import Control.Monad.State

The changes from the final code from step 5 to this module would be performed by a user to implement division.

Add Div constructor to Expr

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr  
>           deriving Show

> type Env = [(Char, Int)]
> type EvalSt = StateT Env

> eval :: (MonadPlus m) => Expr -> EvalSt m Int
> eval (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   lift $ v1 >>= (\x1 -> v2 >>= (\x2 -> return (x1 + x2)))
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   v1 >>= (\x1 -> v2 >>= (\x2 -> return (x1 - x2)))
> eval (Assign x e) = do
>    mv <- eval e
>    env <- get
>    put ((x,mv):env)
>    return mv

The user adds the div case

> eval (Div e1 e2) = do
>   mv1 <- eval e1
>   mv2 <- eval e2
>   mv2 >>= (\v2 -> case v2 of
>                    0         -> mzero 
>                    otherwise -> mv1 >>= (\v1 -> return (v1 `div` v2)))
