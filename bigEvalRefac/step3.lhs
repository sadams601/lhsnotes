> module Step3 where
> import Data.Char
> import Control.Monad.Reader
> import Control.Monad.State

Step 3 is to introduce assignment to the evaluator. Presumably this would be done by hand by the programmer.

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           deriving Show

This is the previous definition of an expression. Commented out here to avoid having to rename all the constructors of the refactored Expr definition found below.
data Expr = Var Char
           | N Int
           | Add Expr Expr
           | Sub Expr Expr
           deriving Show

> type Env = [(Char,Int)]
> type EvalSt = StateT Env
 
> eval :: (Monad m) => Expr -> EvalSt m Int
> eval (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)

The new assign case to eval will also be defined by the user.

> eval (Assign x e) = do
>   env <- get
>   v <- eval e
>   put $ (x,v):env
>   return v

