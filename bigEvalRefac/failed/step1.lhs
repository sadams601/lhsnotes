> module Step1 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Reader

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           deriving Show

> type ParseState = State String

> parse :: String -> ParseState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>       where apply '+' = Add
>             apply '-' = Sub

> prsStr = "(((x+2)-4)+(y+x))"

> env1 :: [(Char,Int)]
> env1 = [('x',5),('y',2)]

> type Env = [(Char,Int)]

> type EvalRdr = Reader Env

> eval :: Expr -> EvalRdr Int
> eval (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)

This is our eval code using just the reader monad and the goal is to refactor it to be of type Expr -> StateT (Env Int) Maybe Int.

My proposed refactoring order is:

1. Monad to MonadTrans refactoring: aka transform Reader to ReaderT
2. Generalise ReaderT to MonadTrans t
3. MonadTrans specialized to StateT
4. Introduce Maybe

Since the old eval rdr only specified one of its types the refactored type does the same.

> type EvalRdr_ref m a = ReaderT Env m a

We will now need to refactor eval to work with the new EvalRdr. The new type of eval is generic over monads because there was no other monads in the original eval so we wont unnecessarily specialise.

> eval_ref :: (Monad m) => Expr -> EvalRdr_ref m Int
> eval_ref (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x ==v])
> eval_ref (N n) = return n
> eval_ref (Add e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return (v1 + v2)
> eval_ref (Sub e1 e2) = do
>   v1 <- eval_ref e1
>   v2 <- eval_ref e2
>   return (v1 - v2)

This works as expected. All that needed to happen is changes to the type signature and then renaming the function.

type rName = Reader t_1

Refactors to

type rName_ref m a= ReaderT t_1 m a

This is a standard pattern for WriterT and StateT as well as ReaderT. Whatever the shared state is is the first type followed by the monadic type and finally the result type.

Through this entire chain of refactorings Env should always be the type of the state and the result should always be an Int.
