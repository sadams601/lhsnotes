> module Step2 where
> import Data.Char
> import Control.Monad.Reader
> import Control.Monad.State

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           deriving Show

> type Env = [(Char,Int)]

> type EvalRdr= ReaderT Env

> eval:: (Monad m) => Expr -> EvalRdr m Int
> eval (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)

This is the current state of the world after making eval work with ReaderT rather than just Reader. This step will refactor eval to use StateT rather than ReaderT.

The critical moment in this refactoring will be how to handle the call to ask in the definition for evaluating a variable.

INFRASTRUCTURE IDEA: It will probably be useful to have a function that goes through some AST fragment and selects subexpressions that are of a certain type.
Possible variations of this theme:
   1. Extract subtrees with certain type
   2. Apply function to subtrees with certain type
   3. Search for existance of a subtree with a certain type

How we handle this scenario depends on how we declare the scope of this particular refactoring.

If lets say that this was a refactoring that transformed a member of the MonadState class to another member of the same class (e.g. WriterT to StateT) then we can guarantee that there will be the standard get and put operations.

If this is a refactoring from one transformer to another transformer then "ask" is just some monadic action of type (m Env) to be generalised.

I'm going to continue the refactoring as though it's the second option. The new eval will take in some parameter of type of (Monad m => StateT Env m Env). The refactoring will start by refactoring the type. I'm including a renaming here because this is no longer a reader.

Also need to add import of Control.Monad.State.

> type EvalSt = StateT Env

Rewrite eval to work using the new EvalSt

> eval_ref :: (Monad m) => (StateT Env m Env) -> Expr -> EvalSt m Int
> eval_ref mAct (Var v) = do
>   env <- mAct
>   return (head [val | (x,val) <- env, x==v])
> eval_ref mAct (N n) = return n
> eval_ref mAct (Add e1 e2) = do
>   v1 <- eval_ref mAct e1
>   v2 <- eval_ref mAct e2
>   return (v1 + v2)
> eval_ref mAct (Sub e1 e2) = do
>   v1 <- eval_ref mAct e1
>   v2 <- eval_ref mAct e2
>   return (v1 - v2)

At this point perhaps the user would want to specialise the function because mAct will always be get. This could be an option for the user to provide some subexpression to be inserted at all call points of each "mAct."

> eval_ref2 :: (Monad m) => Expr -> EvalSt m Int
> eval_ref2 (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> eval_ref2 (N n) = return n
> eval_ref2 (Add e1 e2) = do
>   v1 <- eval_ref2 e1
>   v2 <- eval_ref2 e2
>   return (v1 + v2)
> eval_ref2 (Sub e1 e2) = do
>   v1 <- eval_ref2 e1
>   v2 <- eval_ref2 e2
>   return (v1 - v2)


This will be the code taken to step 3...
