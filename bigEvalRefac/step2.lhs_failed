> module Step2 where
> import Control.Monad.Reader
> import Data.Char

I've taken the definition of eval as it was after the refactoring in step1.lhs.

I have removed the _ref suffixes for readability's sake.

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           deriving Show
> type Env = [(Char,Int)]
> type EvalRdr m a = ReaderT Env m a

> eval :: (Monad m) => Expr -> EvalRdr m Int
> eval (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x ==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)


At this point the Reader monad has been modified to a ReaderT monad transformer containing some monadic type m. The next step is to generalise ReaderT further by making eval work with any member of the MonadTrans typeclass.

This is what the reader type would look like.
It should probably be renamed

type EvalRdr_ref t m a = t Env m a

In this function the only ReaderT specific operation that needs to be generalised is ask

ask :: Monad m => ReaderT r m r

in this particular instance

ask :: (Monad m, MonadTrans t) => t Env m Env

For this new eval to work with any MonadTrans we have to pass in some transformer action as a parameter.

 > eval_ref :: (Monad m, MonadTrans t) => (m Env) -> Expr -> t m Int

> eval_ref tAct (Var v) = do
>   env <- tAct
>   return (head [val | (x,val) <- env, x ==v])
> eval_ref tAct (N n) = return n
> eval_ref tAct (Add e1 e2) = do
>   v1 <- eval_ref tAct e1
>   v2 <- eval_ref tAct e2
>   return (v1 + v2)
> eval_ref tAct (Sub e1 e2) = do
>   v1 <- eval_ref tAct e1
>   v2 <- eval_ref tAct e2
>   return (v1 - v2)


============Starting over=====================

I'm going to start by merging the type synonym into the declared type of eval

type EvalRdr m a = ReaderT Env m a

 eval_ref :: (Monad m) => Expr -> ReaderT Env m Int
 eval_ref (Var v) = do
   env <- ask
   return (head [val | (x,val) <- env, x ==v])
 eval_ref (N n) = return n
 eval_ref (Add e1 e2) = do
   v1 <- eval_ref e1
   v2 <- eval_ref e2
   return (v1 + v2)
 eval_ref (Sub e1 e2) = do
   v1 <- eval_ref e1
   v2 <- eval_ref e2
   return (v1 - v2)

The above code typechecks. 
