module QuickcheckExplodes where
import Explodes
import Test.QuickCheck
import qualified Data.DList as D

propEqual :: Int -> [Int] -> Bool
propEqual n lst = let dlst = D.fromList lst
                      e1  = ex0 n dlst
                      res = [ex1 n dlst
                            ,ex2 n dlst
                            ,ex3 n dlst
                            ,ex4 n dlst
                            ,ex5 n dlst
                            ,ex6 n dlst
                            ,ex7 n dlst] in
                    and (map (e1==) res)
  
                            
                      
