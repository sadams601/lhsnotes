> module DataRefact where

Data driven refactorings are refactorings that change the way that program stores, accesses, and computes over its data.
A few general data refactoring types are:

Generalisation to a super-class
Specialisation to a sub-class
Replace data structure

In general these refactorings will follow some very similar patterns:

1. Preconditions
 -- Here it may be useful to break preconditions into "hard" and "soft" preconditions
 -- Hard preconditions are the traditional idea of a precondition
    A hard precondition prevents the refactoring from being applied when the output would either change the behaviour of the program/ produce invalid code.
 -- A soft precondition prevents the refactoring from producing sub-optimal output that, though correct, defeats the purpose of refactoring
    because the output does not improve maintainability or extensibility over the original program
    a good example of this is not rewriting to applicative if the order that computations occur differs from how they appear in the returned value
       
2. Transformation
 -- It seems important here to separate the idea of "inside" and "outside" the refactoring.
 -- "Inside" the refactoring refers to the function or set of function that have its data changed
 -- "Outside" the refactoring would be anything not inside of the refactoring. For our purposes however what we really care about is the parts of the program
    that directly call the inside of the refactoring

For example:

> main = do
>   tree <- getTree   
>   let enum = enumerate tree
>   print enum

> data Tree a = Leaf
>             | Node (Tree a) a (Tree a)

> enumerate :: Tree a -> [a]
> enumerate Leaf = []
> enumerate (Node left x right) = (enumerate left) ++ [x] ++ (enumerate right)

> getTree = return (Node Leaf 10 Leaf)

If we refactor enumerate to use hughes lists instead of the lists "inside" of this refactoring just consists of the definition of enumerate. "Outside" of the refactoring
formally includes the other three definitions (main, getTree, and Tree) but for HaRe's API we really only care about the main function because it calls enumerate.
Outside of the refactoring is broken up into two different parts, related definitions (in this case main because it calls enumerate) and unrelated definitions (getTree and Tree).
The related definitions will have to be modified to handle the hughes lists that enumerate returns after the refactoring.

Not every data driven refactoring will affect its related functions since not every refactoring will change the type of the program. For example rewriting a program to use the applicative interface
instead of the monadic one does not change the type of the program since every monad is already an applicative.

An API for Data driven refactorings
===================================

At this point there seems to be enough patterns to the data refactorings that we can begin to design and develop an API to support implementing these types of refactorings.

Preconditions
-------------

This is the hardest part to find patterns for. Many of the preconditions are very specific to the types that the refactoring focuses on.

Transforming "Inside" the refactoring
-------------------------------------

Need to come up with some way of generalising the mapping of input syntax to output syntax. Some sort of DSL may be required

Transforming "Outside" the refactoring
--------------------------------------

This part seems much more straightforward. I've already started working on functions that apply other functions at the call points of functions that are
inside the refactoring or wrap parts of the ast with other calls e.g. enumerate tree ==> toList (enumerate tree).
