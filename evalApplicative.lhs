{-# LANGUAGE FlexibleContexts, ConstraintKinds #-}

> module EvalApplicative where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> data Expr = Var Char
>          | N Int
>          | Neg Expr
>          | Add Expr Expr
>          | Sub Expr Expr
>          | Assign Char Expr
>          | Div Expr Expr
>          deriving Show

> type ParseState = State String

parse doesn't want to be written applicatively. 
Not sure what's causing the issue.

I would think the first case should work out to be:

| isAlpha(ch) = Var <$> put chs

Var :: Char -> Expr
put :: MonadState s m => s -> m ()

I think I have to decompose the string within the applicative chain rather than through pattern matching.

> parse :: String -> ParseState Expr
> parse str@(ch:chs)
>   | isAlpha(ch) =  pure (Var ch) <* put chs
>   | isDigit(ch) = pure (N (fromEnum ch - fromEnum '0')) <* put chs

The operator case is definitely not applicative because of how the gets are threaded back into parses.

>   | ch == '(' = do
>       e1 <- parse(chs)
>       (op:rest) <- get
>       e2 <- parse(rest)
>       (')':restFinal) <- get
>       put restFinal
>       return $ apply op e1 e2

>   | ch == '-' = Neg <$> parse chs
>

This is a bit of a different way to do the applicative style. Since the restFinal variable appears in both the left and right hand sides this means that those two lines must be monadic but nothing else in this case is. Instead of keeping the entire case in a do block the two lines in particular can be wrapped into an inline do block. This could perhaps be an alternative mode to the refactoring where short bits of monadic code could be wrapped in a do block.
                 
>   | ch == '<' = let (v:':':rest) = chs in
>     Assign v <$> (parse(rest))
>   where apply '+' = Add
>         apply '-' = Sub
>         apply '/' = Div

| isDigit(ch) = pure $ (N . read . head  <* put . tail) str 

parse :: String -> ParseState Expr
parse (ch:chs)
  | isAlpha(ch) = do
    put chs
    return (Var ch)
  | isDigit(ch) = do
    put chs
    return (N (fromEnum ch - fromEnum '0'))
  | ch == '(' = do
    e1 <- parse(chs)
    rest1 <- get
    let (op:rest2) = rest1
    e2 <- parse(rest2)
    rest3 <- get
    let (')':restFinal) = rest3
    put restFinal
    return $ apply op e1 e2
  | ch =='-' = do
    e <- parse(chs)
    return (Neg e) 
  | ch =='<' = do
    let (v:':':rest) = chs
    e <- parse(rest)
    return (Assign v e)
      where apply '+' = Add
            apply '-' = Sub
            apply '/' = Div

prsStr = "(((<x:5+2)-4)+(<y:2+x))"
divStr = "(4/2)"


env1 = []

> type Env a = [(Char, a)]

> type EvalSt a = State (Env a) a

eval :: Expr -> EvalSt Int
eval (Var v) = do 
  env <- get
  let res = (head [val | (x,val) <- env, x==v]) 
  return res

The first eval case res was rewritten using a lambda in a let clause

> eval :: Expr -> EvalSt Int
> eval (Var v) = let res = (\env -> (head [val | (x,val) <- env, x==v])) in
>   res <$> get
> eval (N n) = pure n

eval (N n) = return n

eval (Neg e) = do
  v <- eval e
  return (-v)

> eval (Neg e) = (\v -> -v) <$> eval e

eval (Add e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  return (v1 + v2)

> eval (Add e1 e2) = (+) <$> eval e1 <*> eval e2

eval (Sub e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  return (v1 - v2)

> eval (Sub e1 e2) = (-) <$> eval e1 <*> eval e2

eval (Div e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  return (v1 `div` v2)

> eval (Div e1 e2) = div <$> eval e1 <*> eval e2

eval (Assign x e) = do
  env <- get
  v <- eval e
  put $ (x,v):env
  return v

 eval (Assign x e) = get >>= (\env -> eval e >>=
                                      (\v -> (put $ (x,v):env) >> return v))

The assign case is pretty tricky. I'm not sure if it can really be written applicatively because "v" needs to be threaded into two places,
the env and as the return value. 

It might require a helper function that causes the side effect and passes on v

 eval (Assign x e) = help <*> (eval e) <*> get
   where help v env = do
           put ((x,v):env)
           return v

