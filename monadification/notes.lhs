> module Notes where
> import Control.Monad.Identity

This file will contains some example refactorings for adding monads to pure code.

Starting with the simple evaluator


> data Expr = N Int
>           | Add Expr Expr
>           | Sub Expr Expr

> eval :: Expr -> Int
> eval (N n) = n
> eval (Add e1 e2) = (eval e1) + (eval e2)
> eval (Sub e1 e2) = (eval e1) - (eval e2)

The case version of eval, this would be code that would be used in many of the monadified versions 

> evalCase :: Expr -> Int
> evalCase e = case e of
>   (N n) -> n
>   (Add e1 e2) -> (eval e1) + (eval e2)
>   (Sub e1 e2) -> (eval e1) - (eval e2)

Interestingly when looking at the core code that is generated they are almost identical

Rec {
eval [Occ=LoopBreaker] :: Expr -> Int
[GblId, Arity=1, Str=DmdType]
eval =
  \ (ds_dAk :: Expr) ->
    case ds_dAk of _ [Occ=Dead] {
      N n_ame -> n_ame;
      Add e1_amf e2_amg ->
        + @ Int GHC.Num.$fNumInt (eval e1_amf) (eval e2_amg);
      Sub e1_anf e2_ang ->
        - @ Int GHC.Num.$fNumInt (eval e1_anf) (eval e2_ang)
    }
end Rec }

evalCase :: Expr -> Int
[GblId, Arity=1, Str=DmdType]
evalCase =
  \ (e_anh :: Expr) ->
    case e_anh of _ [Occ=Dead] {
      N n_ani -> n_ani;
      Add e1_anj e2_ank ->
        + @ Int GHC.Num.$fNumInt (eval e1_anj) (eval e2_ank);
      Sub e1_anl e2_anm ->
        - @ Int GHC.Num.$fNumInt (eval e1_anl) (eval e2_anm)
    }

Possible monadic types with names from the "Monadification as a refactoring" webpage:
Within these definitions use of the standard eval function avoids just duplicating the definition of the eval and evalCase

"Full call-by-value monadification"

> evalM1 :: Monad m => m (Expr -> Int)
> evalM1 = return evalCase

"Restricted call-by-name"

> evalM2 :: Monad m => m Expr -> m Int
> evalM2 em = em >>= (\e -> return (evalCase e))

Either "Data-directed" or "Restricted call-by-value" monadification

> evalM3 :: Monad m => Expr -> m Int
> evalM3 (N n) = return n
> evalM3 (Add e1 e2) = do
>   v1 <- evalM3 e1
>   v2 <- evalM3 e2
>   return (v1 + v2)
> -- or (Add e1 e2) = evalM3 e1 >>= (\v1 -> evalM3 >>= (\v2 -> return (v1 + v2)))
> evalM3 (Sub e1 e2) = do
>   v1 <- evalM3 e1
>   v2 <- evalM3 e2
>   return (v1 - v2)
> -- or (Sub e1 e2) = evalM3 e1 >>= (\v1 -> evalM3 >>= (\v2 -> return (v1 - v2)))

"Full call-by-name"

> evalM4 :: Monad m => m (m Expr -> m Int)
> evalM4 = return evalM2


