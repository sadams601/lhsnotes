> module Simple where

 f1 :: [a] -> b -> c

> f1 [] y = y
> f1 x y = f1 (g x) (h y)

f2 x y z = f2 z y (g x (h y))

                     BIND: Var: f2                     
                           |                           
                          App                          
                           |                           
            ---------------------------                
           /                           \               
          App                         App              
           |                           |               
        -----------             ---------------        
       /           \           /               \       
       App        Var: y       App             App      
       |                       |               |       
    -------                 -------         -------    
   /       \               /       \       /       \   
Var: f2  Var: z          Var: g  Var: x  Var: h  Var: y

> g :: [a] -> [a]
> g x = tail x

> h :: a -> a
> h x = id x


All monadified functions need to be pointed rather than point free



             BIND: Var: f1             
                   |                   
                  App                  
                   |                   
            -------------------        
           /                   \       
          App                 App      
           |                   |       
    -----------             -------    
   /           \           /       \   
Var: f1       App        Var: h  Var: y
               |                       
            -------                    
           /       \                   
         Var: g  Var: x


refactoring h is simple
> h_m :: Monad m => a -> m a
> h_m x = return (id x)

REFACTORING f1

> f1_m1 :: Monad m => [a] -> b -> m b
> f1_m1 [] y = return y
> f1_m1 x y = (h_m y) >>= (\v1 -> f1_m1 (g x) v1)

 f1 [] y = y
 f1 x y = f1 (g x) (h y)

Top down left to right traversal. Descend the left most child, if it's a call to a monadified function then no need to wrap this call with return

Need to keep a reference to that top most app because that holds the shape for the innermost expression.
Need a queue so for every rhs that also calls a monadified function is replaced with a variable and the variable and the syntax that has been monadified gets pushed onto the queue

(HsApp lhs rhs)
isMonadifiedFunCall? rhs = True
then UPDATE (HsApp lhs newVar)
     PUSH (newVar, rhs)
     REWRITE rhs >>= (\ newVar-> (HsApp lhs newVar))
     RECURSIVE call on rhs

TODO: lets

let takes the form

(HsLet [bnd_1, ..., bnd_n] body)


Refactoring function has to take the form of

monadifyExpr :: (ParsedLExpr -> RefactGhc ParsedLExpr)

Step 1: Strip monad calls out of the original RHS replaced with variables
variable and removed ast pushed onto queue. NOTE strip mondad needs to keep track if it's found the left most child
probably need a special case for this. Additionally it should be manually gmap'ed over the given expression so that the search 
starts at 

At this point f1 looks like

             BIND: Var: f1             
                   |                   
                  App                  
                   |                   
            -------------------        
           /                   \       
          App                 Var: hare1     
           |                        
    -----------                 
   /           \            
Var: f1       App       
               |                       
            -------                    
           /       \                   
         Var: g  Var: x



And the queue contains the single entry

("hare1", App )
           |
        ------- 
       /       \
     Var: h   Var: y
