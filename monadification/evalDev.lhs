> module EvalDev where

The goal of this module is to try and approximate developing an evaluator for the following datatype

> data Expr = Num Int
>           | BinOp Op Expr Expr

> data Op = Add
>         | Sub
>         | Mul
>         | Div

> evalOp :: Op -> (Int -> Int -> Int)
> evalOp Add = (+)
> evalOp Sub = (-)
> evalOp Mul = (*) 
> evalOp Div = div

> eval :: Expr -> Int
> eval (Num n) = n
> eval (BinOp op e1 e2) = (evalOp op) (eval e1) (eval e2)

Monadification of the top level eval

> evalM :: Monad m => Expr -> m Int
> evalM (Num n) = return n
> evalM (BinOp op e1 e2) = do
>   v1 <- evalM e1
>   v2 <- evalM e2
>   return ((evalOp op) v1 v2)

