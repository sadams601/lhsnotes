> module TwoFuncMonadification where
> import Data.List

This is a module that solves the highly divisable triangular number problem from Project Euler
https://projecteuler.net/problem=12

> divides :: Int -> Int -> Bool
> divides d n = rem n d == 0

> ld :: Int -> Int
> ld n = ldf 2 n

> ldf :: Int -> Int -> Int
> ldf k n 
>  | divides k n = k
>  | k^2 > n = n
>  | otherwise = ldf (k+1) n
> 
> factors :: Int -> [Int]
> factors n 
>    | n < 1 = error "Invalid argument: argument must be positive"
>    | n == 1 = []
>    | otherwise = p : factors (div n p) where p = ld n
> 
> countDivisors :: [[Int]] -> Int
> countDivisors [] = 1
> countDivisors [x] = (length x) + 1
> countDivisors (x:xs) = ((length x) + 1) * countDivisors xs
> 
> numDivisors :: Int -> Int
> numDivisors k = let f = group (sort (factors k))
>                     in countDivisors f
> 
> triangleNum :: Int -> Int
> triangleNum n = sum [1..n]
> 
> divisorsOfTriangleNums :: Int -> Int
> divisorsOfTriangleNums n =
>                        let tri = triangleNum n
>                        in if numDivisors tri > 500
>                           then tri
>                           else divisorsOfTriangleNums (n + 1)

Just monadifying the top level function

> divisorsOfTriangleNumsM :: Monad m => Int -> m Int
> divisorsOfTriangleNumsM n = do
>   let tri = triangleNum n
>   if numDivisors tri > 500
>     then return tri
>     else divisorsOfTriangleNumsM (n+1)

Instead lets monadify divisorsOfTriangleNums, numDivisors, and countDivisors

> countDivisorsM :: Monad m => [[Int]] -> m Int
> countDivisorsM [] = return 1
> countDivisorsM [x] = return ((length x) + 1)
> countDivisorsM (x:xs) = do
>   v <- countDivisorsM xs
>   return (((length x) + 1) * v)

> numDivisorsM :: Monad m => Int -> m Int
> numDivisorsM k = do
>   let f = group (sort (factors k))
>   countDivisorsM f

The only difference between this definition and the previous monadified version is that part of the predicate
for the if statement is now monadic as well
this value was bound to the variable p and then inserted into the predicate

> divisorsOfTriangleNumsM2 :: Monad m => Int -> m Int
> divisorsOfTriangleNumsM2 n = do
>   let tri = triangleNum n
>   p <- numDivisorsM tri
>   if p > 500
>     then return tri
>     else divisorsOfTriangleNumsM2 (n+1)
