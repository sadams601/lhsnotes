> module StateGen where
> import Data.Char
> import Control.Monad.State
> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type Env a = [(Char, a)]

> type EvalSt a = State (Env a) a


I'm going to be working with the eval function from my simple eval project. First I will rewrite eval to work for type Expr -> EvalSt (Maybe Int) in the most idiomatic way possible. Then I want to refactor eval to work under any monad. Then I want to look at writing eval to use the StateT monad transformer and look at generalisations of that code.

> evalM :: Expr -> EvalSt (Maybe Int)
> evalM (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])

> evalM (N n) = return (return n)

> evalM (Neg e) = do
>   mv <- evalM e
>   return $ mv >>= (\v -> return (-v))

> evalM (Add e1 e2) = do
>   mv1 <- evalM e1
>   mv2 <- evalM e2
>   return $ mv1 >>=
>     (\v1 -> mv2 >>= (\v2 -> return (v1 + v2)))

> evalM (Sub e1 e2) = do
>   mv1 <- evalM e1
>   mv2 <- evalM e2
>   return $ mv1 >>=
>     (\v1 -> mv2 >>= (\v2 -> return (v1 - v2)))

> evalM (Div e1 e2) = do
>   mv1 <- evalM e1
>   mv2 <- evalM e2
>   return $ mv1 >>= 
>      (\v1 -> mv2 >>= 
>              (\v2 -> case v2 of
>                       0 -> Nothing
>                       otherwise -> return (v1 `div` v2)))

> evalM (Assign x e) = do
>  env <- get
>  mv <- evalM e
>  put ((x,mv):env)
>  return mv

It might be more typical to use explicit calls to Just rather than returns but it will save an additional parameter.

> evalM_ref1 :: (Monad m) => (m Int) -> Expr -> EvalSt (m Int)
> evalM_ref1 _ (Var v) = do
>   env <- get
>   return (head [val | (x,val) <- env, x==v])
> 
> evalM_ref1 mAct (Neg e) = do
>   mv <- evalM_ref1 mAct e
>   return $ mv >>= (\v -> return (-v))
> 
> evalM_ref1 mAct (Add e1 e2) = do
>   mv1 <- evalM_ref1 mAct e1
>   mv2 <- evalM_ref1 mAct e2
>   return $ mv1 >>=
>     (\v1 -> mv2 >>=
>             (\v2 -> return (v1 + v2)))
> evalM_ref1 mAct (Sub e1 e2) = do
>   mv1 <- evalM_ref1 mAct e1
>   mv2 <- evalM_ref1 mAct e2
>   return $ mv1 >>=
>     (\v1 -> mv2 >>=
>             (\v2 -> return (v1 - v2)))
> 
> evalM_ref1 mAct (Div e1 e2) = do
>   mv1 <- evalM_ref1 mAct e1
>   mv2 <- evalM_ref1 mAct e2
>   return $ mv1 >>=
>     (\v1 -> mv2 >>=
>             (\v2 -> case v2 of
>                      0 -> mAct
>                      otherwise -> return (v1 `div` v2)))
> 
> evalM_ref1 mAct (Assign x e) = do
>   env <- get
>   mv <- evalM_ref1 mAct e
>   put ((x,mv):env)
>   return mv

> type EvalT = StateT (Env Int)

This is a version of eval using the state monad transformer instead of just the state monad.

> evalT :: Expr -> EvalT Maybe Int
> evalT (Var v) = do
>   env <- get
>   return $ return (head [val | (x,val) <- env, x==v])
> evalT (Neg e) = do
>   mv <- evalT e
>   mv >>= (\v -> return (-v))
