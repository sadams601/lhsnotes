> module IntroMonads where
> import Data.Functor.Identity

This module will attempt to refactor pure functions to use monads.

> mergesort :: (Ord a) => [a] -> [a]
> mergesort [] = []
> mergesort [x] = [x]
> mergesort xs = merge (mergesort fst) (mergesort snd)
>   where fst = take (length xs `div` 2) xs
>         snd = drop (length xs `div` 2) xs

> merge :: (Ord a) => [a] -> [a] -> [a]
> merge xs [] = xs
> merge [] ys = ys
> merge (x:xs) (y:ys)
>    | (x <= y) = x:(merge xs (y:ys))
>    | otherwise = y:(merge (x:xs) ys)


We can introduce the Identity monad into mergesort because there are no client functions.

> idMergesort :: (Ord a) => [a] -> Identity [a]
> idMergesort [] = return []
> idMergesort [x] = return [x]
> idMergesort xs = do
>   x <- idMergesort fst
>   y <- idMergesort snd
>   return $ merge x y
>     where fst = take (length xs `div` 2) xs
>           snd = drop (length xs `div` 2) xs

If we wanted this in the applicative style...

> appMergesort :: (Ord a) => [a] -> Identity [a]
> appMergesort [] = pure []
> appMergesort [x] = pure [x]
> appMergesort xs = merge <$> (appMergesort fst) <*> (appMergesort snd)
>   where fst = take (length xs `div` 2) xs
>         snd = drop (length xs `div` 2) xs


If we refactor merge then the whole module can be made monadic

> idMerge :: (Ord a) => [a] -> [a] -> Identity [a]
> idMerge xs [] = return xs
> idMerge [] ys = return ys
> idMerge (x:xs) (y:ys)
>    | (x <= y) = do
>        ls <- idMerge xs (y:ys)
>        return (x:ls)
>    | otherwise = do
>        ls <- idMerge (x:xs) ys
>        return (y:ls)
>
> idMergesort2 :: (Ord a) => [a] -> Identity [a]
> idMergesort2 [] = return []
> idMergesort2 [x] = return [x]
> idMergesort2 xs = do
>   x <- idMergesort2 fst
>   y <- idMergesort2 snd
>   idMerge x y
>     where fst = take (length xs `div` 2) xs
>           snd = drop (length xs `div` 2) xs

Applicative mergesort...

> appMerge :: (Ord a) => [a] -> [a] -> Identity [a]
> appMerge xs [] = pure xs
> appMerge [] ys = pure ys
> appMerge (x:xs) (y:ys)
>   | (x <= y) = (x:) <$> appMerge xs (y:ys)
>   | otherwise = (y:) <$> appMerge (x:xs) ys

> 
> appMergesort2 :: (Ord a) => [a] -> Identity [a]
> appMergesort2 [] = pure []
> appMergesort2 [x] = pure [x]
> appMergesort2 xs = merge <$> appMergesort2 fst <*> appMergesort2 snd
>   where fst = take (length xs `div` 2) xs
>         snd = drop (length xs `div` 2) xs 

a -> f a
pure f <*> ...
==
f <$> ...

appMerge :: (Ord a) => Identity [a] -> Identity [a] -> Identity [a] 


This is a bit tricky because the type of appMerge does not follow the typical applicative pattern of being either entirely inside the Idiom e.g. appMerger :: (Ord a) => Identity ([a] -> [a] -> [a]) then in mergsort you could use (<*>) on line 79. Instead I had to use the completely pure version of merge and lift it into the Idiom

Now that the monadic structure is in place you can just change the type of the program to use a different monad. For example:

> ioMerge :: (Ord a) => [a] -> [a] -> IO [a]
> ioMerge xs [] = return xs
> ioMerge [] ys = return ys
> ioMerge (x:xs) (y:ys)
>    | (x <= y) = do
>        ls <- ioMerge xs (y:ys)
>        return (x:ls)
>    | otherwise = do
>        ls <- ioMerge (x:xs) ys
>        return (y:ls)
>
> ioMergesort :: (Ord a) => [a] -> IO [a]
> ioMergesort [] = return []
> ioMergesort [x] = return [x]
> ioMergesort xs = do
>   x <- ioMergesort fst
>   y <- ioMergesort snd
>   ioMerge x y
>     where fst = take (length xs `div` 2) xs
>           snd = drop (length xs `div` 2) xs

The programmer can now add whatever relevent io calls they would like.
