> module JulkaSOJ where
> import Control.Monad.IO.Class

Below is the code I used to solve the Sphere Online Judge problem called "Julka," see http://www.spoj.com/problems/JULKA/
I'm going to refactor it to work in any monad 

> main :: IO ()
> main = do
>    --testCaseNum <- readAInt
>    testCaseStrings <- getTestCaseStrings 10 []
>    let result = getResultLines testCaseStrings
>    printLines result

> readAInt :: IO Integer
> readAInt = readLn

> printLines :: (Show a) => [(a,a)] -> IO ()
> printLines [] = return ()
> printLines ((x1,x2):xs) = do 
>    print x1
>    print x2
>    printLines xs

> getTestCaseStrings :: Integer -> [(Integer,Integer)] -> IO [(Integer,Integer)]
> getTestCaseStrings n lst = do 
>    if n == 0
>       then return lst
>       else do
>          line1 <- getLine
>          line2 <- getLine
>          let finalTuple = (read line1, read line2)
>          getTestCaseStrings (n-1) (lst ++ [finalTuple])

> toInt :: String -> Integer
> toInt = read

> getResultLines :: [(Integer,Integer)] -> [(Integer,Integer)]
> getResultLines lst = map getRes lst 

> getRes (total, diff) = (diff+half, half)
>   where half = div (total-diff) 2


There are four functions that currently work with IO and may have to be refactored, main, readAInt, printLines, and getTestCaseStrings.

This is also where some of the scope issues are going to come to play. For instance we could just refactor main by lifting the each of the other function calls using liftIO.

> main_ref1 :: (MonadIO m) => m ()
> main_ref1 = do
>   testCaseStrings <- liftIO $ getTestCaseStrings 10 []
>   let result = getResultLines testCaseStrings
>   liftIO $ printLines result

The downside of using liftIO is that we can only generlise to MonadIO not any monad.

There is also a decent argument that we probably shouldn't generalise the main function because this needs to have type IO (). Instead we would generalise the three other functions.

We could also generalise them by making the io action a parameter instead of just lifting everywhere. For example the refactoring of readAInt looks like this:

> readAInt_ref2 :: m Integer -> m Integer
> readAInt_ref2 mAct = mAct

This is really simple but it makes a little more sense for the other functions.

The show constraint can be dropped here. How could we detect that this constraint is not required automatically? Are there any consequences from removing this constraint?
Dropping looks like the right descision it is limiting the generalness of the function. E.G. writing binary to a database would not require the type to be showable
> printLines_ref2 :: (Show a, Monad m) => (a -> m ()) -> [(a,a)] -> m ()
> printLines_ref2 mAct [] = return ()
> printLines_ref2 mAct ((x1,x2):xs) = do 
>    mAct x1
>    mAct x2
>    printLines_ref2 mAct xs


> getTestCaseStrings_ref2 :: (Monad m) => m String -> Integer -> [(Integer,Integer)] -> m [(Integer,Integer)]
> getTestCaseStrings_ref2 mAct n lst = do 
>    if n == 0
>       then return lst
>       else do
>          line1 <- mAct
>          line2 <- mAct
>          let finalTuple = (read line1, read line2)
>          getTestCaseStrings_ref2 mAct (n-1) (lst ++ [finalTuple])

Then we can write main as

> main_ref2 :: IO ()
> main_ref2 = do
>    --testCaseNum <- readAInt
>    testCaseStrings <- getTestCaseStrings_ref2 getLine 10 []
>    let result = getResultLines testCaseStrings
>    printLines_ref2 print result


This is pretty nice because then main can be written to read and write from any source. Consider the pseudocode

main :: IO ()
main = do
   testCaseStrings <- getTestCaseStrings_ref2 readLineFromPort 10 []
   let result = getResultLines testCaseStrings
   printLines_ref2 writeToDB result

Obviously this particular example doesn't work but, in theory, this code could be reading and writing from any source.

In this case we are generalising the type IO a to (Monad m) => m a. Will this method work for generalising any types?

