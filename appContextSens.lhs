Looking at parsing context sensitive languages with applicative
Taken from https://byorgey.wordpress.com/2012/01/05/parsing-context-sensitive-languages-with-applicative/

> import Text.Parsec hiding ((<|>))
> import Text.Parsec.String
> import Control.Arrow ((&&&))
> import Control.Applicative --hiding ((<|>))
> import Data.List (group)

> guard' :: Alternative f => Bool -> f ()
> guard' True = pure ()
> guard' False = empty

> parseArbitrary :: (String -> Bool) -> Parser ()
> parseArbitrary p = (eof <* guard' (p []))
>                    <|> foldr (<|>) parserZero
>                    (map (\c -> char c *>
>                                parseArbitrary (p . (c:))
>                         )
>                     (['a'..'z']++['0'..'9'])
>                    )

> f :: String -> Bool
> f s
>   | [na,nb,nc]
>     <- map length. group $ s
>        = na == nb && nb == nc
>   | otherwise = False

> g :: String -> Bool
> g s
>    | (c:cs) <- s = length cs == (read [c])  
>    | otherwise = False
>                  
> p = parseArbitrary f
> q = parseArbitrary g
>
> main = do
>   parseTest p "aaa"
>   parseTest p "aaabbbcc"
>   parseTest p "aaaabcccc"
>   putStrLn "Starting successful parse"
>   parseTest p "aaaaabbbbbccccc"
