> module GenPure where
> import Control.Monad

The goal of this module is to work through examples of generalising functions that take pure arguments 
and produce computations within the Maybe monad

First example a 'safe' division that will return Nothing if the denominator is zero

> safeDiv :: Int -> Int -> Maybe Int
> safeDiv n d = 
>  case d of
>     0 -> Nothing
>     _ -> Just (n `div` d)

The obvious generalisation

> safeDiv_refactored :: (Monad m) => Int -> Int -> m Int
> safeDiv_refactored n d =
>  case d of
>     0 -> fail "Divide by zero"
>     _ -> return (n `div` d)

Here we simply replace the call to Just with a call to return and the call to Nothing with fail.
This (http://stackoverflow.com/questions/8163852/should-i-avoid-using-monad-fail) discussion on SO indicates that calling
fail is probably a bad idea.

Alternatives? User provides us with an alternative for Nothing? Some default value for a, identity element.

From Data.Maybe

> listToMaybe           :: [a] -> Maybe a
> listToMaybe []        =  Nothing
> listToMaybe (a:_)     =  Just a

> listToMaybeRef :: (Monad m, Monoid n) => [n] -> m n
> listToMaybeRef []       = return mempty
> listToMaybeRef (a:_)    = return a     

First thought monoid but I think this is very restrictive. 

A user provided default is an alternative though then can't have unconstrained type variables.

> intsToMaybeRef :: (Monad m, Num n) => [n] -> m n
> intsToMaybeRef []    = return 0
> intsToMaybeRef (a:_) = return a       

There is also constraining m to MonadPlus rather than just monad.

> listToMonadPlus :: (MonadPlus m) => [n] -> m n
> listToMonadPlus []    = mzero
> listToMonadPlus (a:_) = return a


Rewriting the safeDiv function using the three different styles I outlined above.

1. a becomes monoid, not feasible because Int is not a monoid

2. Default value works in this case because we have specific types rather than variables.

> safeDiv_defaultVal :: (Monad m) => Int -> Int -> m Int
> safeDiv_defaultVal n d =
>   case d of
>     0 -> return 0
>     _ -> return (n `div` d)

This approach is alright in this case. Though any divide by zeros will evaluate to zero now. The user could provide a different value if they wanted which may be preferable depending on how they want divide by zeros to be handled.

3. MonadPlus approach

> safeDiv_mPlus :: (MonadPlus m) => Int -> Int -> m Int
> safeDiv_mPlus n d =
>   case d of
>     0 -> mzero
>     _ -> return (n `div` d)

Example taken from S. Thompson's Haskell the craft of functional programming pg. 339.

> mapMaybe :: (a -> b) -> Maybe a -> Maybe b
> mapMaybe g Nothing  = Nothing
> mapMaybe g (Just x) = Just (g x)

Collapse this case?

> mapMaybe_ref :: (Monad m) => (a -> b) -> m a -> m b
> mapMaybe_ref g ma = ma >>= (\x -> return (g x))

Code from Learn you a Haskell a fistful of monads chapter

> type Birds = Int  
> type Pole = (Birds,Birds)

> landLeft :: Birds -> Pole -> Maybe Pole  
> landLeft n (left,right)  
>    | abs ((left + n) - right) < 4 = Just (left + n, right)  
>    | otherwise                    = Nothing  
  
> landRight :: Birds -> Pole -> Maybe Pole  
> landRight n (left,right)  
>    | abs (left - (right + n)) < 4 = Just (left, right + n)  
>    | otherwise                    = Nothing  

Here we have pure values

> landLeft_mPlus :: (MonadPlus m) => Birds -> Pole -> m Pole
> landLeft_mPlus n (left,right)
>   | abs ((left+n) - right) < 4 = return (left + n, right)
>   | otherwise                  = mzero

According to Learn You a Haskell's a fistful of monads chapter the MonadPlus class is for monads that can also
act as monoids.

