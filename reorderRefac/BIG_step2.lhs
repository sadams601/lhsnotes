> module Step2 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String
> type FoundVars = Set Char
> type VarsWriter = WriterT FoundVars ParseState

> parse :: String -> ExceptT String VarsWriter Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     liftWriter $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = case res of
>     (Left err) -> Left err
>     (Right e) -> Right (e, fVars)
>     where error = parse s
>           writer = runExceptT error
>           state = runWriterT writer
>           (res, fVars) = evalState state ""

> liftState :: ParseState a -> ExceptT String VarsWriter a
> liftState = lift . lift

> liftWriter :: VarsWriter a -> ExceptT String VarsWriter a
> liftWriter = lift

After extracting the lift synonyms we can start modifying the transformer stack

It seems that the more idiomatic way of handling this is to derive one of the standard typeclasses e.g. MonadState

see http://stackoverflow.com/questions/9054731/avoiding-lift-with-monad-transformers for more details

Lets say though we wanted to move State to the top of the transformer stack so that our code doesn't need calls to liftState in it.

Probably the easiest way to do this is swap the place of except and state in the stack

Probably want to rename this type to ParseStateT

> type ParseState_ref = StateT String
> type VarsWriter_ref = WriterT FoundVars (Except String)

I need to think about what the parameters are to this refactoring. Can it just be a type 'swap'?

The helper functions need to be rewritten as well

> liftWriter_ref :: VarsWriter_ref a -> ParseState_ref VarsWriter_ref a
> liftWriter_ref = lift
 
The types are going to be really tricky here. What is actually happening?


> parse_ref :: String -> ParseState_ref VarsWriter_ref Expr
> parse_ref (ch:chs)
>    | isAlpha(ch) = do
>      put chs
>      liftWriter_ref $ tell $ singleton ch
>      return (Var ch)
>    | isDigit(ch) = do
>      put chs
>      return (N (fromEnum ch - fromEnum '0'))
>    | ch == '(' = do
>      e1 <- parse_ref(chs)
>      rest1 <- get
>      let (op:rest2) = rest1
>      e2 <- parse_ref(rest2)
>      rest3 <- get
>      let (')':restFinal) = rest3
>      put restFinal
>      return $ apply op e1 e2
>    | ch =='-' = do
>      e <- parse_ref(chs)
>      return (Neg e) 
>    | ch =='<' = do
>      let (v:':':rest) = chs
>      e <- parse_ref(rest)
>      restFinal <- get
>      put restFinal
>      return (Assign v e)
>    | otherwise =
>      lift . lift $ throwError $ "Unknown character found: " ++ [ch]
>        where apply '+' = Add
>              apply '-' = Sub
>              apply '/' = Div
 
Refactoring runParse is going to be really tricky because the order that the run and eval functions need to be applied in will change.

1. evalStateT :: Monad m => StateT s m a -> s -> m a
2. runWriterT :: WriterT w m a -> m (a, w)
3. runExcept  :: Except e a -> Either e a

The type of parse_ref without synonyms

parse_ref :: String -> StateT String (WriterT FoundVars (Except String)) Expr


> runParse_ref :: String -> Either String (Expr, FoundVars)
> runParse_ref s =
>   let state = parse_ref s
>       writer = evalStateT state ""
>       err = runWriterT writer
>       res = runExcept err in
>   case res of
>     (Left err) -> Left err
>     (Right (e,fVars)) -> Right (e, fVars)
