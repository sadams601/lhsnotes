> module Step2 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String
> type FoundVars = Set Char
> type ExceptState = ExceptT String ParseState

> liftState :: ParseState a -> WriterT FoundVars ExceptState a
> liftState = lift . lift
 
> liftExcept :: ExceptState a -> WriterT FoundVars ExceptState a
> liftExcept = lift

> parse :: String -> WriterT FoundVars ExceptState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div
 
> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = res
>     where writer = parse s
>           error  = runWriterT writer
>           state  = runExceptT error
>           res    = evalState state ""             

This step will continue demoting the except monad to the bottom of the stack

> type ParseState_ref = Except String
> type ExceptState_ref = StateT String ParseState_ref
 
> liftState_ref :: ExceptState_ref a -> WriterT FoundVars ExceptState_ref a
> liftState_ref = lift

> liftExcept_ref :: ParseState_ref a -> WriterT FoundVars ExceptState_ref a
> liftExcept_ref = lift . lift

> parse_ref :: String -> WriterT FoundVars ExceptState_ref Expr
> parse_ref (ch:chs)
>   | isAlpha(ch) = do
>     liftState_ref $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState_ref $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse_ref(chs)
>     rest1 <- liftState_ref $ get
>     let (op:rest2) = rest1
>     e2 <- parse_ref(rest2)
>     rest3 <- liftState_ref $ get
>     let (')':restFinal) = rest3
>     liftState_ref $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse_ref(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse_ref(rest)
>     restFinal <- get
>     liftState_ref $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept_ref $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

> runParse_ref :: String -> Either String (Expr, FoundVars)
> runParse_ref s = either
>   where writer = parse_ref s
>         state  = runWriterT writer
>         err    = evalStateT state ""
>         either = runExcept err        
