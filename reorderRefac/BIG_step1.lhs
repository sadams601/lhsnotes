> module Step1 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String
> type FoundVars = Set Char
> type VarsWriter = WriterT FoundVars ParseState

> parse :: String -> ExceptT String VarsWriter Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     liftWriter $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

Writing a helper function that gets the parsed expression and the found variables

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = case res of
>     (Left err) -> Left err
>     (Right e) -> Right (e, fVars)
>     where error = parse s
>           writer = runExceptT error
>           state = runWriterT writer
>           (res, fVars) = evalState state ""

The first step is to extract lift functions

> liftState :: ParseState a -> ExceptT String VarsWriter a
> liftState = lift . lift

> liftWriter :: VarsWriter a -> ExceptT String VarsWriter a
> liftWriter = lift
