> module Step1 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String
> type FoundVars = Set Char
> type VarsWriter = WriterT FoundVars ParseState

> parse :: String -> ExceptT String VarsWriter Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     liftWriter $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

Writing a helper function that gets the parsed expression and the found variables

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = case res of
>     (Left err) -> Left err
>     (Right e) -> Right (e, fVars)
>     where error = parse s
>           writer = runExceptT error
>           state = runWriterT writer
>           (res, fVars) = evalState state ""

> liftState :: ParseState a -> ExceptT String VarsWriter a
> liftState = lift . lift

> liftWriter :: VarsWriter a -> ExceptT String VarsWriter a
> liftWriter = lift
 
The goal of this refactoring is to modify the transformer stack so that state is the top level transformer and the exception monad is at the bottom.

Each step will involve switching each type one position at a time. For this transformation the stack starts from top to bottom as

ExceptT
WriterT 
State

The refactoring will start by demoting the except monad to the bottom then promoting State to the top of the stack.

Step one swaps the order of ExceptT and WriterT

> type VarsWriter_ref = ExceptT String ParseState

This type synonym will really need to be renamed.

The lift functions will need to be modified as well

> liftState_ref :: ParseState a -> WriterT FoundVars VarsWriter_ref a
> liftState_ref = lift . lift
 
> liftWriter_ref :: VarsWriter_ref a -> WriterT FoundVars VarsWriter_ref a
> liftWriter_ref = lift

Then we go through parse modifying the calls to lift/lifting functions as appropriate

> parse_ref :: String -> WriterT FoundVars VarsWriter_ref Expr
> parse_ref (ch:chs)
>   | isAlpha(ch) = do
>     liftState_ref $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState_ref $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse_ref(chs)
>     rest1 <- liftState_ref $ get
>     let (op:rest2) = rest1
>     e2 <- parse_ref(rest2)
>     rest3 <- liftState_ref $ get
>     let (')':restFinal) = rest3
>     liftState_ref $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse_ref(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse_ref(rest)
>     restFinal <- get
>     liftState_ref $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftWriter_ref $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

Rewriting runParse

> runParse_ref :: String -> Either String (Expr, FoundVars)
> runParse_ref s = res
>     where writer = parse_ref s
>           error  = runWriterT writer
>           state  = runExceptT error
>           res    = evalState state ""
