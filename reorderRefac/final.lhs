> module Final where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

This file represents a more final product after several renamings occur.

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show
 
> type FoundVars = Set Char   
> type ParseState = Except String
> type ParserWriter = WriterT FoundVars ParseState

> liftWriter :: ParserWriter a -> StateT String ParserWriter a
> liftWriter = lift

> liftExcept :: ParseState a -> StateT String ParserWriter a
> liftExcept = lift . lift

> parse :: String -> StateT String ParserWriter Expr 
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     liftWriter $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div


> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = either
>   where state  = parse s
>         writer = evalStateT state ""
>         err    = runWriterT writer
>         either = runExcept err 
 
