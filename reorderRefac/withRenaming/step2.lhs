> module Step2 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

--> type ParseState = State String

> type FoundVars = Set Char

-- > type ExceptState = ExceptT String ParseState

-- > liftState :: ParseState a -> WriterT FoundVars ExceptState a
-- > liftState = lift . lift

-- > liftExcept :: ExceptState a -> WriterT FoundVars ExceptState a
-- > liftExcept = lift

-- > parse :: String -> WriterT FoundVars ExceptState Expr
-- > parse (ch:chs)
-- >   | isAlpha(ch) = do
-- >     liftState $ put chs
-- >     tell $ singleton ch
-- >     return (Var ch)
-- >   | isDigit(ch) = do
-- >     liftState $ put chs
-- >     return (N (fromEnum ch - fromEnum '0'))
-- >   | ch == '(' = do
-- >     e1 <- parse(chs)
-- >     rest1 <- liftState $ get
-- >     let (op:rest2) = rest1
-- >     e2 <- parse(rest2)
-- >     rest3 <- liftState $ get
-- >     let (')':restFinal) = rest3
-- >     liftState $ put restFinal
-- >     return $ apply op e1 e2
-- >   | ch =='-' = do
-- >     e <- parse(chs)
-- >     return (Neg e) 
-- >   | ch =='<' = do
-- >     let (v:':':rest) = chs
-- >     e <- parse(rest)
-- >     restFinal <- get
-- >     liftState $ put restFinal
-- >     return (Assign v e)
-- >   | otherwise =
-- >     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
-- >       where apply '+' = Add
-- >             apply '-' = Sub
-- >             apply '/' = Div

-- > runParse :: String -> Either String (Expr, FoundVars)
-- > runParse s = res
-- >     where writer = parse s
-- >           error  = runWriterT writer
-- >           state  = runExceptT error
-- >           res    = evalState state ""

Again the user will be demoting except. This time will involve transforming ExceptT to Except and State will become StateT.

The refactoring starts by modifying  ParseState and ExceptState with the user providing new names for both types

> type ParseExcept = Except String
> type ParseState = StateT String ParseExcept
 
Our lift functions change again. Two levels down we now have ParseExcept and one level down is ParseState, again HaRe will prompt the user for new names.

> liftState :: ParseState a -> WriterT FoundVars ParseState a
> liftState = lift

> liftExcept :: ParseExcept a -> WriterT FoundVars ParseState a
> liftExcept = lift . lift

> parse_ref :: String -> WriterT FoundVars ParseState Expr
> parse_ref (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse_ref(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse_ref(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse_ref(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse_ref(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = either
>   where writer = parse_ref s
>         state  = runWriterT writer
>         err    = evalStateT state ""
>         either = runExcept err
