> module Step3 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show
 
> type FoundVars = Set Char                    
> type ParseExcept = Except String

Now the user will promote StateT one level this means changing the ParseState type. The user also renames at this point.

> type VarsWriter = WriterT FoundVars ParseExcept

The exception lift function only needs the result type changed so the user decides to keep the same name.

> liftExcept :: ParseExcept a -> StateT String VarsWriter a
> liftExcept = lift . lift
 
The other lift function gets renamed because it now lifts VarsWriter instead of a State type

> liftWriter :: VarsWriter a -> StateT String VarsWriter a
> liftWriter = lift

The parse function can now be refactored 

> parse :: String -> StateT String VarsWriter Expr 
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     liftWriter $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div
 
> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = either
>   where state  = parse s
>         writer = evalStateT state ""
>         err    = runWriterT writer
>         either = runExcept err              
