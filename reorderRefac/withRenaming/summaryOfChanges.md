This is just a quick summary of the changes that were made during the reordering refactoring.

## Step 1

The first step is to demote the ExceptT one level down the stack.

At the end of this step parse has type

`parse :: String -> WriterT FoundVars ExceptState Expr`

1. Modify type originally known as VarsWriter to be:
  * `type VarsWriter_ref = ExceptT String ParseState`
2. User renames this type to something more suitable like `ExceptState`
3. The type that needs to be lifted two levels remains `ParseState` but HaRe will probably need the user to confirm that the name should remain the same
4. The type that needs to be lifted one level is now `ExceptState` so `liftWriter` gets renamed to `liftExcept`
5. Refactor `parse`
6. Refactoring `runParse`

## Step 2

Again we will be moving Except down a level in the transformer stack.

This time parse is of type:

`parse :: String -> WriterT FoundVars ParseState Expr`

1. The first step will involve demoting Except out of the ExceptState type into the ParseState type and promoting State to the ExceptState type.
  * `type ParseState_ref = Except String`
  * `type ExceptState_ref = StateT String ParseState_ref`
2. Rename `ParseState_ref` to `ParseExcept`
3. Rename `ExceptState_ref` to `ParseState`
4. The `lift . lift` synonym now needs to lift type `ParseExcept`
  * Rename this synonym to `liftExcept`
5. The `lift` synonym needs to lift `ParseState` 
  * Rename this synonym `liftExcept`
6. Refactor `parse`
7. Refactor `runParse`

## Step 3

This time we promote StateT up a level

Parse has type

`parse :: String -> StateT String VarsWriter Expr`

1. ParseState becomes
  * `type ParseState_ref = WriterT FoundVars ParseExcept`
2. Rename `ParseState_ref` to `VarsWriter`
3. The `lift . lift` synonym still lifts the same type so a renaming isn't neccesary
4. The `lift` synonym lifts VarsWriter now 
  * Rename this to `liftWriter`
5. Refactor `parse`
6. Refactor `runParse`

Refactoring `parse` in general involves finding any monadic type on the RHS and prepending that type with the appropriate number of lifts/ lift synonym.