> {-# LANGUAGE FlexibleContexts #-}
> module RunParseNotes where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show
                    
> type FoundVars = Set Char                       

> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     put restFinal
>     return (Assign v e)
>   | otherwise =
>     throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div
 

For the three monads Except, Writer, and State there are three different orderings

From top to bottom

1.
State
Writer
Except

parse :: String -> StateT String (WriterT FoundVars (Except String)) Expr

> runParse1 :: String -> Either String (Expr, FoundVars)
> runParse1 s = either
>   where state  = parse s
>         writer = evalStateT state ""
>         err    = runWriterT writer
>         either = runExcept err   

2.
State
Writer
Except

parse :: String -> StateT String (ExceptT String (Writer FoundVars)) Expr

> runParse2 :: String -> Either String (Expr, FoundVars)
> runParse2 s = case either of
>   (Left s) -> (Left s)
>   (Right e) -> (Right (e,fvars))
>   where state = parse s
>         err = evalStateT state ""
>         writer = runExceptT err
>         (either, fvars) = runWriter writer

3.
Writer
State 
Except

parse :: String -> WriterT FoundVars (StateT String (Except String)) Expr

> runParse3 :: String -> Either String (Expr, FoundVars)
> runParse3 s = either
>   where writer = parse s
>         state  = runWriterT writer
>         err    = evalStateT state ""
>         either = runExcept err
         
4.
Writer
Except
State

parse :: String -> WriterT FoundVars (ExceptT String (State String)) Expr

> runParse4 :: String -> Either String (Expr, FoundVars)
> runParse4 s = res
>     where writer = parse s
>           error  = runWriterT writer
>           state  = runExceptT error
>           res    = evalState state ""

5.
Except
Writer
State

parse :: String -> ExceptT String (WriterT FoundVars (State String)) Expr

> runParse5 :: String -> Either String (Expr, FoundVars)
> runParse5 s = case res of
>     (Left err) -> Left err
>     (Right e) -> Right (e, fVars)
>     where error = parse s
>           writer = runExceptT error
>           state = runWriterT writer
>           (res, fVars) = evalState state ""

6.
Except
State
Writer

parse :: String -> ExceptT String (StateT String (Writer FoundVars)) Expr

> runParse6 :: String -> Either String (Expr, FoundVars)
> runParse6 s = case either of
>   (Left s) -> (Left s)
>   (Right e) -> (Right (e,fvars))
>   where err = parse s
>         state = runExceptT err
>         writer = evalStateT state ""
>         (either, fvars) = runWriter writer
