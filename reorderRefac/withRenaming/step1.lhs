> module Step1 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show

> type ParseState = State String
> type FoundVars = Set Char

-- > type VarsWriter = WriterT FoundVars ParseState

-- > parse :: String -> ExceptT String VarsWriter Expr
-- > parse (ch:chs)
-- >   | isAlpha(ch) = do
-- >     liftState $ put chs
-- >     liftWriter $ tell $ singleton ch
-- >     return (Var ch)
-- >   | isDigit(ch) = do
-- >     liftState $ put chs
-- >     return (N (fromEnum ch - fromEnum '0'))
-- >   | ch == '(' = do
-- >     e1 <- parse(chs)
-- >     rest1 <- liftState $ get
-- >     let (op:rest2) = rest1
-- >     e2 <- parse(rest2)
-- >     rest3 <- liftState $ get
-- >     let (')':restFinal) = rest3
-- >     liftState $ put restFinal
-- >     return $ apply op e1 e2
-- >   | ch =='-' = do
-- >     e <- parse(chs)
-- >     return (Neg e) 
-- >   | ch =='<' = do
-- >     let (v:':':rest) = chs
-- >     e <- parse(rest)
-- >     restFinal <- get
-- >     liftState $ put restFinal
-- >     return (Assign v e)
-- >   | otherwise =
-- >     throwError $ "Unknown character found: " ++ [ch]
       where apply '+' = Add
             apply '-' = Sub
             apply '/' = Div

 runParse :: String -> Either String (Expr, FoundVars)
 runParse s = case res of
     (Left err) -> Left err
     (Right e) -> Right (e, fVars)
     where error = parse s
           writer = runExceptT error
           state = runWriterT writer
           (res, fVars) = evalState state ""

 liftState :: ParseState a -> ExceptT String VarsWriter a
 liftState = lift . lift

 liftWriter :: VarsWriter a -> ExceptT String VarsWriter a
 liftWriter = lift

I'm doing another version of the reordering refactoring this time explicitly doing renamings between each step.

We want to be able to handle the lift functions as renamings rather than redefining the body of the functions everytime.

For this refactoring the user would start by saying they wanted ExceptT demoted.

This produces the type:

type VarsWriter_ref = ExceptT String ParseState

At this point could HaRe ask the user to provide a better name for this type?

> type ExceptState = ExceptT String ParseState

The type that needs to be lifted two levels is still state. HaRe probably wont be able to infer this without user input saying to keep the same name. 

> liftState :: ParseState a -> WriterT FoundVars ExceptState a
> liftState = lift . lift
 
The type that is being lifted one level is now ExceptT. HaRe asks the user for a new name.
 
> liftExcept :: ExceptState a -> WriterT FoundVars ExceptState a
> liftExcept = lift

Realistically HaRe would also ask about renaming parse as well and in this case the user would probably say to just keep the same name.

> parse :: String -> WriterT FoundVars ExceptState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

Rewriting runParse

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = res
>     where writer = parse s
>           error  = runWriterT writer
>           state  = runExceptT error
>           res    = evalState state ""
