>module Step3 where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Writer
> import Control.Monad.Except
> import Data.Functor.Identity
> import Data.Set

> data Expr = Var Char
>           | N Int
>           | Neg Expr
>           | Add Expr Expr
>           | Sub Expr Expr
>           | Assign Char Expr
>           | Div Expr Expr
>           deriving Show
 
> type FoundVars = Set Char                    

> type ParseState = Except String
> type ExceptState = StateT String ParseState
 
> liftState :: ExceptState a -> WriterT FoundVars ExceptState a
> liftState = lift

> liftExcept :: ParseState a -> WriterT FoundVars ExceptState a
> liftExcept = lift . lift

> parse :: String -> WriterT FoundVars ExceptState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     liftState $ put chs
>     tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     liftState $ put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- liftState $ get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- liftState $ get
>     let (')':restFinal) = rest3
>     liftState $ put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse(rest)
>     restFinal <- get
>     liftState $ put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div

> runParse :: String -> Either String (Expr, FoundVars)
> runParse s = either
>   where writer = parse s
>         state  = runWriterT writer
>         err    = evalStateT state ""
>         either = runExcept err        


Now we can perform the final step lifting StateT to the top of the stack above WriterT

> type ExceptState_ref = WriterT FoundVars ParseState

> liftState_ref :: ExceptState_ref a -> StateT String ExceptState_ref a
> liftState_ref = lift

> liftExcept_ref :: ParseState a -> StateT String ExceptState_ref a
> liftExcept_ref = lift . lift

> parse_ref :: String -> StateT String ExceptState_ref Expr 
> parse_ref (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     liftState_ref $ tell $ singleton ch
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse_ref(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse_ref(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>   | ch =='-' = do
>     e <- parse_ref(chs)
>     return (Neg e) 
>   | ch =='<' = do
>     let (v:':':rest) = chs
>     e <- parse_ref(rest)
>     restFinal <- get
>     put restFinal
>     return (Assign v e)
>   | otherwise =
>     liftExcept_ref $ throwError $ "Unknown character found: " ++ [ch]
>       where apply '+' = Add
>             apply '-' = Sub
>             apply '/' = Div


> runParse_ref :: String -> Either String (Expr, FoundVars)
> runParse_ref s = either
>   where state  = parse_ref s
>         writer = evalStateT state ""
>         err    = runWriterT writer
>         either = runExcept err 
