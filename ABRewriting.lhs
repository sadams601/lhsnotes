> module ABRewrite where

> data A a = Empty
>          | Elem a (A a)
>          deriving (Show, Eq)
>
> instance Foldable A where
>   foldr f x Empty = x
>   foldr f x (Elem i rst) = i `f` (foldr f x rst)

> emptyA :: A a
> emptyA = Empty
>
> singletonA :: a -> A a
> singletonA a = Elem a Empty
>
> appendA :: A a -> A a -> A a
> appendA Empty y = y
> appendA (Elem a x) y = (Elem a (appendA x y))
>
> mapA :: (a -> b) -> A a -> A b
> mapA _ Empty = Empty
> mapA f (Elem x rst) = Elem (f x) (mapA f rst)
>
> consA :: a -> A a -> A a
> consA i lst = Elem i lst
>

> fromList :: [a] -> A a
> fromList [] = Empty
> fromList (x:xs) = consA x (fromList xs)
>
> toList :: A a -> [a]
> toList Empty = []
> toList (Elem i rst) = i:(toList rst)
> 
> reverseA :: A a -> A a
> reverseA Empty = Empty
> reverseA a@(Elem i Empty) = a
> reverseA (Elem i rst) = appendA (reverseA rst) (Elem i Empty) 

> newtype B a = B [a]
>   deriving (Show, Eq)

> instance Foldable B where
>   foldr f x (B lst) = foldr f x lst

> mapB :: (a -> b) -> B a -> B b
> mapB f (B l) = B (map f l)

> appendB :: B a -> B a -> B a
> appendB (B lst1) (B lst2) = B (lst1 ++ lst2)

> singletonB :: a -> B a
> singletonB i = B [i]

> emptyB :: B a
> emptyB = B []

> consB :: a -> B a -> B a
> consB i (B lst) = B (i:lst)
>
> reverseB :: B a -> B a
> reverseB (B lst) = B (reverse lst)



======= Projection and Abstraction functions ====

Projection:

> toB :: A a -> B a
> toB a = B (toList a)

Abstraction:

> toA :: B a -> A a
> toA (B lst) = fromList lst

======= Source function ==========

> f :: Monoid m => (a -> m) -> A a -> m
> f fun a = foldr mappend mempty (mapA fun a)


We want to refactor f to use B instead of A. For this refactoring we associate
mapA and mapB which means that during refactoring any instances of mapA can become mapB.

the refactored version of f :: Monoid m => (a -> m) -> B a -> m

since we are refactoring the second of three types of f we descend the AST to the leaves
that match the parameter to be refactored and move up the tree from there.

So we descend to:

     LHS1                       RHS1
App (mapA fun :: (A a -> A m)) (a :: A a)

The variable "a" in RHS1 will become the type of (B a) post refactoring but this new type cannot be applied to LHS1 without a type error so we look at that expression next with the goal of rewriting it to become something of type (B a -> _):

     LHS2                             RHS2 
LHS1@(App (mapA :: (a -> b) -> A a -> A b) (fun :: (a -> m)))

Let's say we scan this tree from left to right so starting with LHS2. Now we have something we can replace because the refactoring is defined such that "mapA" is replaceable by "mapB" so we swap out "mapA" for mapB and check the type of the expression.

LHS1_ref(App (mapB :: (a -> b) -> B a -> B b) (fun :: (a -> m))) :: (B a -> B m)

This application's new type matches our desired type of (B a -> _) so the next application type checks as well:

(App (mapB fun :: (B a -> B m)) (a :: B a)) :: B m

Now we continue up the tree making sure that at every higher step typechecks (which it does) so that we end up with the final function definition of:

> f_ref :: Monoid m => (a -> m) -> B a -> m
> f_ref fun a = foldr mappend mempty (mapB fun a)


=================== Source function 2 ==================

> addAtEnd :: a -> A a -> A a
> addAtEnd i lst = reverseA (consA i (reverseA lst))

> addAtEnd_ref :: a -> B a -> A a
> addAtEnd_ref i lst = toA (reverseB (consB i (reverseB lst)))
