> module MoneyParser where
> import Control.Applicative hiding ((<|>), many, optional)
> import Control.Monad
> import Text.ParserCombinators.Parsec

The goal of this module is to create an applicative parser that parses strings representing money amounts of the form:
<currency symbol><whole amount>.<decimal amount> where the fullstop and decimal amount are optional.

Money is represented by these data types:

> data Currency = Dollar
>               | Pound
>               | Euro
>                 deriving (Show, Eq)


> data Money = M Currency Integer Integer
>              deriving (Show, Eq)

Here's a small helper function that reads strings into integers. This eliminates the need to wrap parsed values with reads inside the parser itself.

> mkMoney :: Currency -> String -> String -> Money
> mkMoney c w d = M c (read w) (read d)

This parser when written monadically would take the form of:

> parseMoneyM :: CharParser () Money
> parseMoneyM = do
>    currency <- parseCurrency
>    whole <- many1 digit
>    decimal <- (option "0" (do { 
>                            char '.';
>                            d <- many1 digit;
>                            return d}))
>    return $ M currency (read whole) (read decimal)

> parseCurrency :: CharParser () Currency
> parseCurrency = parsePound <|> parseDollar <|> parseEuro
>    where parsePound = Pound <$ char '£'
>          parseDollar = Dollar <$ char '$'
>          parseEuro = Euro <$ char '€'


> parseMoney :: CharParser () Money
> parseMoney = M <$> parseCurrency <*> readWhole <*> readDecimal
>              where readWhole = read <$> many1 digit
>                    readDecimal = read <$> option "0" (char '.' *> many1 digit)
>                    

> parseMoney_ref :: CharParser () Money
> parseMoney_ref = (pure M) <*> parseCurrency <*> (read <$> many1 digit) <*> (read <$> option "0" (char '.' *> many1 digit))

I should write a function that converts any other currency to pounds to test having multiple 'pure' functions on the lhs of the computation.
Use this function to test parenthesis/lambda formats

> convertToDollars :: Money -> Money
> convertToDollars m@(M Dollar _ _) = m
> convertToDollars (M Pound pounds pence) = convertCurrency 1.46 (toRational pounds) pence
> convertToDollars (M Euro euro cents) = convertCurrency 1.14 (toRational euro) cents

> convertCurrency rate w d = let total = w + ((toRational d) * 10^^(-2))
>                                newTot = total * rate
>                                (newW,dec) = properFraction newTot in
>                            M Dollar newW $ truncate (dec * 100) 
>                                                                                

> parseDollarsM :: CharParser () Money
> parseDollarsM = do
>   currency <- parseCurrency
>   whole <- many1 digit
>   decimal <- (option "0" (do {
>                             char '.';
>                             d <- many1 digit;
>                             return d
>                             }))
>   return $ convertToDollars $ M currency (read whole) (read decimal)

> parseDollars :: CharParser () Money
> parseDollars = convertToDollars <$> (mkMoney <$> parseCurrency <*> many1 digit <*> option "0" (char '.' *> many1 digit))
>

 parseDollarsM = do
    currency <- parseCurrency
    whole <- many1 digit
    decimal <- (option "0" (do {
                                  char '.';
                                  d <- many1 digit; return d
                               }))
    return $ convertToDollars $ M currency (read whole) (read decimal)

Interesting example that has to introduce a lambda but can still be written applicatively

look at lets

>{-
> f = do
>   number <- parseNum
>   y <- makeY
>   return $ (number,g number, y)
>
> f_ref = (\n y-> (n, g n,y)) <$> parseNum <*> makeY
> -}
