> module MaybeToMaybeGen where
> import Control.Monad

The goal of this module is to provide examples of generalising functions which contain the Maybe type as both input and output.

From Data.Generics.Aliases

> orElse :: Maybe a -> Maybe a -> Maybe a
> x `orElse` y = case x of
>                  Just _  -> x
>                  Nothing -> y



> orElse_ref :: (MonadPlus m) => m a -> m a -> m a
> x `orElse_ref` y = case x of
>                      mzero -> y
>                      _     -> x

Tricky case as I had to swap the ordering of the cases. This is due to MonadPlus' ability to pattern match on mzero. However there is no generic way to unwrap a monad like the (Just _) pattern match does. Swapping the arguments allows for the non-mzero case to just be a wildcard
                      
Example taken from S. Thompson's Haskell the craft of functional programming pg. 339.

> mapMaybe :: (a -> b) -> Maybe a -> Maybe b
> mapMaybe g Nothing  = Nothing
> mapMaybe g (Just x) = Just (g x)

> mapMaybe_ref :: (Monad m) => (a -> b) -> m a -> m b
> mapMaybe_ref g ma = ma >>= (\x -> return (g x))


