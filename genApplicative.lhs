> module GenApplicative where
> import Control.Applicative hiding ((<|>), many, optional)
> import Control.Monad
> import Text.ParserCombinators.Parsec
> import Data.Map

http://www.serpentine.com/blog/2008/02/06/the-basics-of-applicative-functors-put-to-practical-work/

Here's a pretty good introduction to applicative based on examples rather than theory like many of the other resources

The minimal complete definition of a Applicative requires two functions to be defined, "pure" and (<*>)

Pure lifts a value into the applicative

pure :: a -> f a

and (<*>) is a sequential application

(<*>) :: f (a->b) -> f a -> f b

There are two other functions that are derived automatically (*>) and (<*) which sequence actions and discard the left and right value respectively

From the link above

 instance Applicative (GenParser s a) where
   pure = return
   (<*>) = ap

 instance Alternative (GenParser s a) where
   empty = mzero
   (<|>) = mplus

(>>=) :: forall a b. m a -> (a -> m b) -> m b

ap :: Monad m => m (a -> b) -> m a -> m b

The default implementation of (<*>) is just ap

I think there are two interesting refactorings here
   1. Generisizing non-monadic instances of applicative e.g. (,) a when a is a monoid
   2. Rewriting monadic code that is actually applicative


Let's take a step back and ask ourselves what is Applicative? It provides a way to lift a value into the functor with "pure" and the infix operator (<*>) which applies some function to some value, both the function and value are within the same applicative context.

WHEN IS USING BIND ACTUALLY JUST USING (<*>) ????

From http://stackoverflow.com/questions/7040844/applicatives-compose-monads-dont

Take the two snippets

> miffy :: Monad m => m Bool -> m x -> m x -> m x
> miffy mb mt mf = do
>   b <- mb
>   if b then mt else mf

and

> iffy :: Applicative a => a Bool -> a x -> a x -> a x
> iffy ab at af = pure cond <*> ab <*> at <*> af where
>   cond b t f = if b then t else f

Not sure if this example is particularliy meaningful though I'm unsure how this matches the precondition

> pmiffy :: Monad m => Bool -> m x -> m x -> m x
> pmiffy b mt mf = do
>   if b then mt else mf

> piffy :: Applicative a => Bool -> a x -> a x -> a x
> piffy b at af = cond b <$> at <*> af
>   where cond b t f = if b then t else f
                                
The monadic code (miffy) allows the value of the monadic computation mb determine whether to return the result of computation mt or mf without running the computation that wasn't chosen.

Iffy on the other hand will return the same expected value but both ab and at will be run regardless of result of ab.

In other words applicative functions will have all of the computation steps run regardless of the results of previous computations in the chain.

More discussion of this can be found here in terms of the "past" and "future" parts of the computation 
http://stackoverflow.com/questions/23342184/difference-between-monad-and-applicative-in-haskell

Refactoring one: generalising non-monadic instances of Applicative

There are two instances of Applicative that I can find that are not also an instance of monad

instance Monoid a => Applicative ((,) a) where
   pure x = (mempty, x)
   (u, f) <*> (v, x) = (u `mappend` v, f x)


and

ZipList

instance Applicative ZipList where
    pure x = ZipList (repeat x)
    ZipList fs <*> ZipList xs = ZipList (zipWith id fs xs)

I don't think this refactoring is that feasible as ziplist would only be written in applicative style because the only thing that differentiates it from a normal list is its apply behavior and the tuple is also so specific that I can't really imagine it being used in a non-applicative way. Additionally I'm not convinced a generically applicative function is that useful. Without much in the way of control flow you can apply to applicative what good is a general function that just passes functions a values through calls to apply?

On the other hand generisising monadic computations seems much more usable because every monad is also applicative.

When can a bind be rewritten as an apply?

The most basic case is:

> m :: Monad m => m (a -> b) -> m a -> m b
> m mf ma = do
>   f <- mf
>   a <- ma
>   return $ f a

Or using bind syntax

> m2 :: Monad m => m (a -> b) -> m a -> m b
> m2 mf ma = mf >>=
>    (\f -> ma >>=
>           (\a -> return (f a)))

In general the things that need to happen are: pull function from monadic context, pull value from monadic context, return value applied to function

> data JSON = JArr [JSON]
>           | JObj (Map String JSON)
>           | JStr String
>           | JNum Double
>           | JBool Bool
>           | JNull
>             deriving (Show, Eq)

type JsonParse = CharParser () JSON

> parseJSON :: String -> Either ParseError JSON
> parseJSON input = parse jsonParse "none" input

> jsonParse :: CharParser () JSON
> jsonParse = spaces *> (parseJObj <|> parseJArr)
>    where parseJObj = JObj <$> (char '{' *> spaces *> parseObjEntries <* spaces <* char '}')
>          parseJArr = JArr <$> (char '[' *> spaces *> parseJVal `sepByIgnoreSpaces` char ',' <* spaces <* char ']' <* spaces)
>          parseObjEntries = fromList <$> objEntry `sepByIgnoreSpaces` char ','
>          objEntry = (,) <$> (spaces *> parseStr <* spaces <* char ':' <* spaces) <*> (parseJVal <* spaces)
> 
> parseJVal = JBool <$> parseBool
>             <|> JNum <$> parseDouble
>             <|> JNull <$ string "null"
>             <|> JStr <$> parseStr
>
> parseDouble :: CharParser () Double
> parseDouble = (\x -> read <$> (x++)) <$> many1 digit <*> decimal
>    where decimal = option "" $ (:) <$> char '.' <*> many1 digit
>

This is where strings are currently parsed. Right now it's broken because it should be able to accept properly escaped characters but it's much simpler to just disallow additional double quotes.

> parseStr :: CharParser () String 
> parseStr = char '"' *> (many1 (noneOf "\"")) <* char '"'

Created a new combinator that works the same as sepBy but ignores spaces on either side of the seperator

> sepByIgnoreSpaces :: CharParser () a -> CharParser () sep -> CharParser () [a]
> sepByIgnoreSpaces p sep = p `sepBy` (try (spaces *> sep <* spaces))

the double parsing code was adapted from https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/parsing-floats-with-parsec


> parseBool :: CharParser () Bool
> parseBool = parseTrue <|> parseFalse
>    where parseTrue = True <$ string "true"
>          parseFalse = False <$ string "false"
