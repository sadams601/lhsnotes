> module MaybeTrans where
> import Control.Monad
> import Control.Monad.Trans.Maybe
> import Data.Char
> import Control.Monad.Trans.Class

import Control.Applicative

Taken from https://en.wikibooks.org/wiki/Haskell/Monad_transformers

> getValidPassphrase :: MaybeT IO String
> getValidPassphrase = do s <- lift getLine
>                         guard (isValid s)
>                         return s

> isValid :: String -> Bool
> isValid s = length s >= 8
>             && any isAlpha s
>             && any isNumber s
>             && any isPunctuation s

> askPassphrase :: MaybeT IO ()
> askPassphrase = do lift $ putStrLn "Insert your new passphrase"
>                    value <- getValidPassphrase
>                    lift $ putStrLn "Storing in database..."

I think this generlises really nicely. I don't see anything that explicitly requires this code to run in MaybeT.

getValidPassphrase_ref1 :: (MonadTrans t, MonadPlus t) => t IO String

getValidPassphrase_ref1 = do s <- lift getLine
                              guard (isValid s)
                              return s

Ah nevermind generlising MaybeT in this case would still require the type to be a member of MonadTrans because of the lifts and Alternative because of the guard
