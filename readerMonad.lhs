> module ReaderMonad where
> import Data.Char
> import Control.Monad.State
> import Debug.Trace
> import Control.Monad.Reader

> data Expr = Var Char
>           | N Int
>           | Add Expr Expr
>           | Sub Expr Expr
>           deriving Show

> type ParseState = State String

> parse :: String -> ParseState Expr
> parse (ch:chs)
>   | isAlpha(ch) = do
>     put chs
>     return (Var ch)
>   | isDigit(ch) = do
>     put chs
>     return (N (fromEnum ch - fromEnum '0'))
>   | ch == '(' = do
>     e1 <- parse(chs)
>     rest1 <- get
>     let (op:rest2) = rest1
>     e2 <- parse(rest2)
>     rest3 <- get
>     let (')':restFinal) = rest3
>     put restFinal
>     return $ apply op e1 e2
>       where apply '+' = Add
>             apply '-' = Sub

> prsStr = "(((x+2)-4)+(y+x))"

> env1 :: [(Char,Int)]
> env1 = [('x',5),('y',2)]

> type Env = [(Char,Int)]

> type EvalRdr = Reader Env

> eval :: Expr -> EvalRdr Int
> eval (Var v) = do
>   env <- ask
>   return (head [val | (x,val) <- env, x==v])
> eval (N n) = return n
> eval (Add e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 + v2)
> eval (Sub e1 e2) = do
>   v1 <- eval e1
>   v2 <- eval e2
>   return (v1 - v2)

Removed the assign case because this is the reader monad
eval (Assign x e) = do
   liftIO $ putStrLn ("Assigning: " ++ [x])
   v <- eval e
   local (\env -> (x,v):env) $ eval e


This is sort of interesting since the type of eval isn't Expr -> Reader Env Int the refactoring will have to modify the EvalRdr type as well. I guess that you can't add typeclass constraints see: http://stackoverflow.com/questions/12717301/haskell-type-synonym-declaration-with-constraint-possible

There used to be a pragma to allow constraints (DatatypeContexts) which has been removed from the language and according to this SO answer (http://stackoverflow.com/questions/7438600/datatypecontexts-deprecated-in-latest-ghc-why) GADTs were the replacement. Might be worth looking into this.

For now I'm just going to modify evals type instead of having a type synonym as well.

Ah second issue there isn't a typeclass for monads of two types. Instead trying to refactor a ReaderT function.

> type ReadTEval m a = ReaderT Env m a

> evalT :: Expr -> ReadTEval IO Int
> evalT (Var v) = do
>   env <- ask
>   lift $ putStrLn ("Found var named: " ++ [v])
>   return (head [val | (x,val) <- env, x==v])
> evalT (N n) = return n
> evalT (Add e1 e2) = do
>   v1 <- evalT e1
>   v2 <- evalT e2
>   return (v1 + v2)
> evalT (Sub e1 e2) = do
>   v1 <- evalT e1
>   v2 <- evalT e2
>   return (v1 - v2)

This code really has to stay inside of the MonadReader typeclass.

What about an extract type synonym refactoring e.g.

eval :: Expr -> ReaderT Env IO Int

This refactors to

type ReadTEval m a = ReaderT Env m atype

eval :: Expr -> ReadTEval IO Int

> evalT_ref :: (Monad m) => (String -> m a) -> Expr -> ReadTEval m Int
> evalT_ref mAct (Var v) = do
>   env <- ask
>   lift $ mAct ("Found var named: " ++ [v])
>   return (head [val | (x,val) <- env, x==v])
> evalT_ref mAct (N n) = return n
> evalT_ref mAct (Add e1 e2) = do
>   v1 <- evalT_ref mAct e1
>   v2 <- evalT_ref mAct e2
>   return (v1 + v2)
> evalT_ref mAct (Sub e1 e2) = do
>   v1 <- evalT_ref mAct e1
>   v2 <- evalT_ref mAct e2
>   return (v1 - v2)

> castEval :: Expr -> ReadTEval IO Int
> castEval = evalT_ref putStrLn
