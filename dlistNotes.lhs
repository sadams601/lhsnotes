> module DListNotes where
> import qualified Data.DList as D
> import Test.QuickCheck

> s :: [Int] -> Int
> s lst = foldr (+) 0 lst

               App
              /  \
             App lst :: [Int]
            :: [Int] -> Int
            /     \
           App    0 
          /  \
       foldr (+)
       :: (a->b->b) -> b -> [a] -> b

Time to refactor this function

> s_r :: D.DList Int -> Int
> s_r lst = D.foldr (+) 0 lst 

> prop_s :: [Int] -> Bool
> prop_s lst = s lst == s_r (D.fromList lst)

> test_s = quickCheck prop_s

> exponents :: Int -> [Int]
> exponents base = base : (exponents (2*base))

> exponents_r :: Int -> D.DList Int
> exponents_r base = base `D.cons` (exponents_r (2*base))

> prop_e :: Int -> Bool
> prop_e i = let res1 = exponents i
>                res2 = exponents_r i in
>              take 20 res1 == take 20 (D.toList res2)

> addtwo :: [Int] -> [Int]
> addtwo lst = map (2+) lst

> addtwo_r :: D.DList Int -> D.DList Int
> addtwo_r lst = D.map (2+) lst

> prop_a :: [Int] -> Bool
> prop_a lst = addtwo lst == D.toList (addtwo_r (D.fromList lst))
